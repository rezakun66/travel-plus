-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2019 at 11:49 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `harga_tujuan`
--

CREATE TABLE `harga_tujuan` (
  `id_harga` varchar(18) NOT NULL,
  `id_travel` varchar(15) NOT NULL,
  `id_kota` varchar(3) NOT NULL,
  `harga` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_tujuan`
--

INSERT INTO `harga_tujuan` (`id_harga`, `id_travel`, `id_kota`, `harga`) VALUES
('dmi08117585555', '08117585555', 'dmi', '100000'),
('tbh08117585555', '08117585555', 'tbh', '150000');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `mobil` varchar(10) NOT NULL,
  `id_harga` varchar(18) NOT NULL,
  `supir` varchar(15) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `naik_turun_harga` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `mobil`, `id_harga`, `supir`, `tanggal`, `jam`, `naik_turun_harga`) VALUES
(1, 'BM4848JKT', 'tbh08117585555', '0820758550', '2019-05-31', '00:00:00', '-10000'),
(8, 'BM4848JKT', 'tbh08117585555', '0820758551', '2018-12-19', '10:00:00', '-10000'),
(9, 'BM4848JKT', 'dmi08117585555', '0820758550', '2018-12-22', '10:00:00', '-15900'),
(10, 'BM4848JKT', 'tbh08117585555', '0820758551', '2019-05-22', '10:00:00', '-10000'),
(12, 'BM4848JKT', 'dmi08117585555', '0820758550', '2018-12-22', '10:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id_kota` varchar(3) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `nama`, `foto`) VALUES
('dmi', 'dumai', 'fotodmi.jpg'),
('rgt', 'rengat', 'fotorgt.jpg'),
('tbh', 'tembilahan', 'fototbh.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kursi`
--

CREATE TABLE `kursi` (
  `id_kursi` int(11) NOT NULL,
  `id_pesan` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `kursi` varchar(2) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kursi`
--

INSERT INTO `kursi` (`id_kursi`, `id_pesan`, `id_jadwal`, `kursi`, `status`) VALUES
(3, 5, 10, '3', 'sistem'),
(4, 6, 10, 'cc', 'sistem'),
(5, 7, 10, '1', 'batal'),
(6, 8, 12, 'cc', 'sistem');

-- --------------------------------------------------------

--
-- Table structure for table `log_top_up`
--

CREATE TABLE `log_top_up` (
  `id_top_up` int(11) NOT NULL,
  `waktu_top_up` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` varchar(15) NOT NULL,
  `nomor_rekening` varchar(20) NOT NULL,
  `tanggal_transfer` date NOT NULL,
  `jam_transfer` time NOT NULL,
  `jumlah` decimal(11,0) NOT NULL,
  `bukti_transfer` varchar(100) NOT NULL,
  `status` char(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_top_up`
--

INSERT INTO `log_top_up` (`id_top_up`, `waktu_top_up`, `id_user`, `nomor_rekening`, `tanggal_transfer`, `jam_transfer`, `jumlah`, `bukti_transfer`, `status`) VALUES
(3, '2019-01-12 14:22:47', '08117585551', '1234567890', '2019-01-01', '01:00:00', '100000', 'bukti3.jpg', 'ditolak'),
(6, '2019-01-01 15:51:07', '08117585551', '1234567890', '2019-01-01', '12:59:00', '100000', 'bukti4.jpg', 'diproses'),
(7, '2019-01-01 15:56:06', '08117585551', '1234567890', '2019-01-01', '12:59:00', '100000', 'bukti7.jpg', 'diproses'),
(8, '2019-01-01 15:57:07', '08117585551', '1234567890', '2019-01-01', '12:59:00', '100000', 'bukti8.jpg', 'diproses'),
(9, '2019-01-01 15:58:23', '08117585551', '1234567890', '2019-01-01', '12:59:00', '100000', 'bukti9.jpg', 'diproses'),
(10, '2019-01-01 16:13:34', '08117585551', '1234567890', '2019-01-01', '12:59:00', '100000', 'bukti10.jpg', 'diproses'),
(11, '2019-01-01 16:16:07', '08117585551', '1234567890', '2019-01-01', '01:01:00', '100000', 'bukti11.jpg', 'diproses'),
(12, '2019-01-03 16:20:35', '08117585551', '1234567890', '2019-01-03', '01:00:00', '290000', 'bukti12.jpg', 'diproses'),
(13, '2019-01-10 15:44:55', '08117585551', '1234567890', '2019-01-10', '01:00:00', '90000', 'bukti13.jpg', 'diterima'),
(14, '2019-01-20 08:56:59', '08117585551', '1234567890', '2019-01-20', '01:00:00', '290000', 'bukti14.jpg', 'diterima'),
(15, '2019-01-20 09:00:14', '08117585551', '9999999999', '2019-01-19', '01:00:00', '190000', 'bukti15.jpg', 'diproses'),
(16, '2019-01-20 09:23:02', '08117585551', '9999999999', '2019-01-20', '00:00:00', '90000', 'bukti16.jpg', 'diproses'),
(17, '2019-01-20 09:23:35', '08117585551', '1234567890', '2019-01-20', '01:00:00', '90000', 'bukti17.jpg', 'diproses');

-- --------------------------------------------------------

--
-- Table structure for table `mobil`
--

CREATE TABLE `mobil` (
  `flat_mobil` varchar(10) NOT NULL,
  `id_travel` varchar(15) NOT NULL,
  `jenis_mobil` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobil`
--

INSERT INTO `mobil` (`flat_mobil`, `id_travel`, `jenis_mobil`) VALUES
('BM4848JKT', '08117585555', 'KIJANG INOVA');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `saldo` decimal(11,0) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama`, `jenis_kelamin`, `alamat`, `no_telp`, `saldo`, `foto`) VALUES
('08117585551', 'tukul arwana', 'L', 'jl yang sangat jauh sekalii', '08117585551', '1020000', 'foto08117585551.jpg'),
('08117585552', 'kiki', 'L', 'jl amanah', '08117585552', '0', 'foto08117585552.jpg'),
('08117585554', 'koko', 'L', 'jl jauh', '08117585554', '0', 'foto08117585554.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pesan` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `id_pelanggan` varchar(15) NOT NULL,
  `lat_jemput` double NOT NULL,
  `long_jemput` double NOT NULL,
  `alamat_jemput` varchar(100) NOT NULL,
  `pembayaran` decimal(11,0) NOT NULL,
  `waktu_pesan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `waktu_batal` datetime NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id_pesan`, `id_jadwal`, `id_pelanggan`, `lat_jemput`, `long_jemput`, `alamat_jemput`, `pembayaran`, `waktu_pesan`, `waktu_batal`, `status`) VALUES
(5, 10, '08117585551', 0.46279805160369003, 101.36802582652285, 'jl jauhh', '140000', '2019-01-03 14:08:40', '0000-00-00 00:00:00', 'lunas'),
(6, 10, '08117585551', 0.4583525834665932, 101.39409600371266, 'jalan jalan', '140000', '2019-01-03 14:08:40', '0000-00-00 00:00:00', 'lunas'),
(7, 10, '08117585551', 0.47344070173748176, 101.39137177378848, 'shrsdhs', '70000', '2019-01-03 14:08:40', '2019-01-20 07:00:00', 'batal'),
(8, 12, '08117585551', 0.46421810908014666, 101.39137078891486, 'Jl jauh', '100000', '2018-11-30 23:23:09', '0000-00-00 00:00:00', 'lunas');

-- --------------------------------------------------------

--
-- Table structure for table `pencairan_saldo`
--

CREATE TABLE `pencairan_saldo` (
  `id_pencairan` int(11) NOT NULL,
  `waktu_pencairan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_travel` varchar(15) NOT NULL,
  `jumlah_pencairan` decimal(11,0) NOT NULL,
  `tanggal_transfer` date NOT NULL,
  `jam_transfer` time NOT NULL,
  `bukti_transfer` varchar(50) NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `status` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pencairan_saldo`
--

INSERT INTO `pencairan_saldo` (`id_pencairan`, `waktu_pencairan`, `id_travel`, `jumlah_pencairan`, `tanggal_transfer`, `jam_transfer`, `bukti_transfer`, `waktu_selesai`, `status`) VALUES
(3, '2019-01-17 09:43:21', '08117585555', '110000', '2019-01-17', '01:00:00', 'bukti3.jpg', '2019-01-17 16:52:20', 'ditransfer'),
(4, '2019-01-20 10:13:53', '08117585555', '110000', '2019-01-20', '01:00:00', '-', '2019-01-20 17:14:54', 'ditransfer');

-- --------------------------------------------------------

--
-- Table structure for table `supir`
--

CREATE TABLE `supir` (
  `id_supir` varchar(15) NOT NULL,
  `id_travel` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supir`
--

INSERT INTO `supir` (`id_supir`, `id_travel`, `nama`, `jenis_kelamin`, `alamat`, `no_telp`, `foto`) VALUES
('0820758550', '08117585555', 'ujang uhuk', 'L', 'jl becek', '0820758550', 'foto0820758550.jpg'),
('0820758551', '0820758551', 'aluh aji o', 'L', 'jl pangjang ', '0820758551', 'foto0820758551.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE `travel` (
  `id_travel` varchar(15) NOT NULL,
  `nama_travel` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp1` varchar(15) NOT NULL,
  `no_telp2` varchar(15) NOT NULL,
  `no_rekening` varchar(20) NOT NULL,
  `kode_bank` varchar(3) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `saldo` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel`
--

INSERT INTO `travel` (`id_travel`, `nama_travel`, `alamat`, `no_telp1`, `no_telp2`, `no_rekening`, `kode_bank`, `logo`, `saldo`) VALUES
('0811758553', 'cv. cahaya 99', 'jl jauh men', '08117585553', '127', '01958971343876634256', '016', '', '1000000000'),
('08117585550', 'hijau travel', 'jl hijau', '08117585550', '08127585550', '123456789012345', '016', 'logo08117585550.jpg', '0'),
('08117585555', 'indah travel', 'jl subrantas', '08117585555', '0', '123456789012345', '016', 'logo08117585555.jpg', '1250000'),
('08117585556', 'ivan travel', 'jl. emboya', '08117585556', '08127585556', '123456789012345', '016', 'logo08117585556.jpg', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `password`, `level`) VALUES
('08117585550', 'hijau', 'travel'),
('08117585551', 'ikan', 'pelanggan'),
('08117585552', 'ayam', 'pelanggan'),
('08117585554', 'ayam', 'pelanggan'),
('08117585555', 'indah', 'travel'),
('08117585556', 'ivan', 'travel'),
('0820758550', 'ayam', 'supir'),
('0820758551', 'ayam', 'supir'),
('admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `harga_tujuan`
--
ALTER TABLE `harga_tujuan`
  ADD PRIMARY KEY (`id_harga`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `kursi`
--
ALTER TABLE `kursi`
  ADD PRIMARY KEY (`id_kursi`);

--
-- Indexes for table `log_top_up`
--
ALTER TABLE `log_top_up`
  ADD PRIMARY KEY (`id_top_up`);

--
-- Indexes for table `mobil`
--
ALTER TABLE `mobil`
  ADD PRIMARY KEY (`flat_mobil`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `pencairan_saldo`
--
ALTER TABLE `pencairan_saldo`
  ADD PRIMARY KEY (`id_pencairan`);

--
-- Indexes for table `supir`
--
ALTER TABLE `supir`
  ADD PRIMARY KEY (`id_supir`);

--
-- Indexes for table `travel`
--
ALTER TABLE `travel`
  ADD PRIMARY KEY (`id_travel`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kursi`
--
ALTER TABLE `kursi`
  MODIFY `id_kursi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `log_top_up`
--
ALTER TABLE `log_top_up`
  MODIFY `id_top_up` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pencairan_saldo`
--
ALTER TABLE `pencairan_saldo`
  MODIFY `id_pencairan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
