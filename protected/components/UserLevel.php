<?php
class UserLevel {
    const ADMIN = 'admin';
    const TRAVEL = 'travel';
    const PELANGGAN = 'pelanggan';
	const SUPIR = 'supir';
	
    public static function getLabel ($user){
        if ($user == self::ADMIN)
            return ADMIN;
        if ($user == self::TRAVEL)
                return TRAVEL;
        if ($user == self::PELANGGAN)
                return PELANGGAN;
        if ($user == self::SUPIR)
                return SUPIR;
    }
    public static function getList(){
        return array (
        self::ADMIN => self::getLabel(self::ADMIN),
        self::TRAVEL => self::getLabel(self::TRAVEL),
        self::PELANGGAN => self::getLabel(self::PELANGGAN),
        self::SUPIR => self::getLabel(self::SUPIR),

        );
    }
}
?>
