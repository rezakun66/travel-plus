<?php
class EWebUser extends CWebUser{
    protected $_model;

    public function getIsAdmin(){
        return Yii::app()->user->role == UserLevel::ADMIN;
    }

    public function getIsTravel(){
        return Yii::app()->user->role == UserLevel::TRAVEL;
    }
    public function getIsPelanggan(){
        return Yii::app()->user->role == UserLevel::PELANGGAN;
    }
    public function getIsSupir(){
        return Yii::app()->user->role == UserLevel::SUPIR;
    }
    protected function loadUser(){
        if ($this->_model === null){
            $this->_model = User::model()->findByPk($this->id);
        }
        return $this->_model;
    }
    public function getRole(){
        return $this->getState('level');
    }
}
?>
