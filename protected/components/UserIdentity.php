<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user=User::model()->findByAttributes(array('id_user'=>$this->username));// Users adl nama model yg dpt dari table
		//$user=User::model()->find('LOWER(username)=?',array(strtolower($this->username)));
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($user->password!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else{
			$this->username=$user->id_user;
			$this->setState('id', $user->id_user);
			$this->setState('level', $user->level);
			//$this->setState('status', $user->level=='1');
			Yii::app()->session->add('id', $user->id_user);
			Yii::app()->session->add('level', $user->level);
			$user->save();
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
}