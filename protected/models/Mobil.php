<?php

/**
 * This is the model class for table "mobil".
 *
 * The followings are the available columns in table 'mobil':
 * @property string $flat_mobil
 * @property string $id_travel
 * @property string $jenis_mobil
 */
class Mobil extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mobil';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('flat_mobil, id_travel, jenis_mobil', 'required'),
			array('flat_mobil', 'length', 'max'=>10),
			array('id_travel', 'length', 'max'=>15),
			array('jenis_mobil', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('flat_mobil, id_travel, jenis_mobil', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'trav'=>array(self::BELONGS_TO,'Travel','id_travel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'flat_mobil' => 'Flat Mobil',
			'id_travel' => 'Id Travel',
			'jenis_mobil' => 'Jenis Mobil',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('flat_mobil',$this->flat_mobil,true);
		$criteria->compare('id_travel',$this->id_travel,true);
		$criteria->compare('jenis_mobil',$this->jenis_mobil,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mobil the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
