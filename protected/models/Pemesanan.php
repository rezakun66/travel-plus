<?php

/**
 * This is the model class for table "pemesanan".
 *
 * The followings are the available columns in table 'pemesanan':
 * @property integer $id_pesan
 * @property integer $id_jadwal
 * @property string $id_pelanggan
 * @property double $lat_jemput
 * @property double $long_jemput
 * @property string $alamat_jemput
 * @property string $pembayaran
 * @property string $waktu_pesan
 * @property string $waktu_batal
 * @property string $status
 */
class Pemesanan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pemesanan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pesan, id_jadwal, id_pelanggan, pembayaran, status', 'required'),
			array('id_pesan, id_jadwal', 'numerical', 'integerOnly'=>true),
			array('lat_jemput, long_jemput', 'numerical'),
			array('id_pelanggan', 'length', 'max'=>15),
			array('alamat_jemput', 'length', 'max'=>100),
			array('pembayaran', 'length', 'max'=>11),
			array('status', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pesan, id_jadwal, id_pelanggan, lat_jemput, long_jemput, alamat_jemput, pembayaran, waktu_pesan, waktu_batal, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'schedule'=>array(self::BELONGS_TO,'Jadwal','id_jadwal'),
		'customer'=>array(self::BELONGS_TO,'Pelanggan','id_pelanggan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pesan' => 'Id Pesan',
			'id_jadwal' => 'Id Jadwal',
			'id_pelanggan' => 'Id Pelanggan',
			'lat_jemput' => 'Lat Jemput',
			'long_jemput' => 'Long Jemput',
			'alamat_jemput' => 'Alamat Jemput',
			'pembayaran' => 'Pembayaran',
			'waktu_pesan' => 'Waktu Pesan',
			'waktu_batal' => 'Waktu Batal',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pesan',$this->id_pesan);
		$criteria->compare('id_jadwal',$this->id_jadwal);
		$criteria->compare('id_pelanggan',$this->id_pelanggan,true);
		$criteria->compare('lat_jemput',$this->lat_jemput);
		$criteria->compare('long_jemput',$this->long_jemput);
		$criteria->compare('alamat_jemput',$this->alamat_jemput,true);
		$criteria->compare('pembayaran',$this->pembayaran,true);
		$criteria->compare('waktu_pesan',$this->waktu_pesan,true);
		$criteria->compare('waktu_batal',$this->waktu_batal,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
		public function lastPsn(){
		 $criteria=new CDbCriteria;
         $criteria->select='id_pesan';
         $criteria->order='id_pesan DESC';
         $criteria->limit=1;
         $modelPsn=Pemesanan::model()->find($criteria);
		 $valu=$modelPsn->id_pesan+1;
		 return $valu;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pemesanan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
