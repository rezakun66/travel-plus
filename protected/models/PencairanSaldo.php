<?php

/**
 * This is the model class for table "pencairan_saldo".
 *
 * The followings are the available columns in table 'pencairan_saldo':
 * @property integer $id_pencairan
 * @property string $waktu_pencairan
 * @property string $id_travel
 * @property string $jumlah_pencairan
 * @property string $tanggal_transfer
 * @property string $jam_transfer
 * @property string $bukti_transfer
 * @property string $waktu_selesai
 * @property string $status
 */
class PencairanSaldo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pencairan_saldo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' id_travel, jumlah_pencairan, status', 'required'),
			array('id_travel', 'length', 'max'=>15),
			array('jumlah_pencairan', 'length', 'max'=>11),
			array('bukti_transfer', 'length', 'max'=>50),
			array('status', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pencairan, waktu_pencairan, id_travel, jumlah_pencairan, tanggal_transfer, jam_transfer, bukti_transfer, waktu_selesai, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'travel'=>array(self::BELONGS_TO,'Travel','id_travel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pencairan' => 'Id Pencairan',
			'waktu_pencairan' => 'Waktu Pencairan',
			'id_travel' => 'Id Travel',
			'jumlah_pencairan' => 'Jumlah Pencairan',
			'tanggal_transfer' => 'Tanggal Transfer',
			'jam_transfer' => 'Jam Transfer',
			'bukti_transfer' => 'Bukti Transfer',
			'waktu_selesai' => 'Waktu Selesai',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pencairan',$this->id_pencairan);
		$criteria->compare('waktu_pencairan',$this->waktu_pencairan,true);
		$criteria->compare('id_travel',$this->id_travel,true);
		$criteria->compare('jumlah_pencairan',$this->jumlah_pencairan,true);
		$criteria->compare('tanggal_transfer',$this->tanggal_transfer,true);
		$criteria->compare('jam_transfer',$this->jam_transfer,true);
		$criteria->compare('bukti_transfer',$this->bukti_transfer,true);
		$criteria->compare('waktu_selesai',$this->waktu_selesai,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function notif(){
		$model1=PencairanSaldo::model()->findAll(array("condition"=>"status = 'diproses'"));

		$value=0;
		foreach ($model1 as $key) {
			$value++;
		}
		return $value;	
	}

	public function uangKeluar($tgl,$tgl2){

		$timee=" 23:59:59";
		$timee0=" 00:00:00";
		$model1=PencairanSaldo::model()
		->findAll(array(     
        'condition'=>'status="ditransfer" and waktu_selesai between "'.$tgl.$timee0.'" and "'.$tgl2.$timee.'" '
                    ));
		$value=0;
		foreach ($model1 as $key) {
			$value=$value+$key->jumlah_pencairan-10000;
		}
		return $value;	
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PencairanSaldo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
