<?php

/**
 * This is the model class for table "supir".
 *
 * The followings are the available columns in table 'supir':
 * @property string $id_supir
 * @property string $id_travel
 * @property string $nama
 * @property string $jenis_kelamin
 * @property string $alamat
 * @property string $no_telp
 * @property string $foto
 */
class Supir extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supir';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_supir, id_travel, nama, jenis_kelamin, alamat, no_telp', 'required'),
			array('id_supir, id_travel, no_telp', 'length', 'max'=>15),
			array('nama, foto', 'length', 'max'=>50),
			array('jenis_kelamin', 'length', 'max'=>1),
			array('alamat', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_supir, id_travel, nama, jenis_kelamin, alamat, no_telp, foto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_supir' => 'Id Supir',
			'id_travel' => 'Id Travel',
			'nama' => 'Nama',
			'jenis_kelamin' => 'Jenis Kelamin',
			'alamat' => 'Alamat',
			'no_telp' => 'No Telp',
			'foto' => 'Foto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_supir',$this->id_supir,true);
		$criteria->compare('id_travel',$this->id_travel,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('no_telp',$this->no_telp,true);
		$criteria->compare('foto',$this->foto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Supir the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
