<?php

/**
 * This is the model class for table "log_top_up".
 *
 * The followings are the available columns in table 'log_top_up':
 * @property integer $id_top_up
 * @property string $waktu_top_up
 * @property string $id_user
 * @property string $nomor_rekening
 * @property string $tanggal_tranfer
 * @property string $jam_tranfer
 * @property string $jumlah
 * @property string $bukti_tranfer
 * @property string $status
 */
class LogTopUp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_top_up';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, nomor_rekening, tanggal_tranfer, jam_tranfer, jumlah, status', 'required'),
			array('id_user', 'length', 'max'=>15),
			array('nomor_rekening', 'length', 'max'=>20),
			array('jumlah', 'length', 'max'=>11),
			array('bukti_tranfer', 'length', 'max'=>100),
			array('status', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_top_up, waktu_top_up, id_user, nomor_rekening, tanggal_tranfer, jam_tranfer, jumlah, bukti_tranfer, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customer'=>array(self::BELONGS_TO,'Pelanggan','id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_top_up' => 'Id Top Up',
			'waktu_top_up' => 'Waktu Top Up',
			'id_user' => 'Id User',
			'nomor_rekening' => 'Nomor Rekening',
			'tanggal_tranfer' => 'Tanggal Tranfer',
			'jam_tranfer' => 'Jam Tranfer',
			'jumlah' => 'Jumlah',
			'bukti_tranfer' => 'Bukti Tranfer',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_top_up',$this->id_top_up);
		$criteria->compare('waktu_top_up',$this->waktu_top_up,true);
		$criteria->compare('id_user',$this->id_user,true);
		$criteria->compare('nomor_rekening',$this->nomor_rekening,true);
		$criteria->compare('tanggal_tranfer',$this->tanggal_tranfer,true);
		$criteria->compare('jam_tranfer',$this->jam_tranfer,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('bukti_tranfer',$this->bukti_tranfer,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogTopUp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
