<?php

/**
 * This is the model class for table "jadwal".
 *
 * The followings are the available columns in table 'jadwal':
 * @property integer $id_jadwal
 * @property string $mobil
 * @property string $id_harga
 * @property string $supir
 * @property string $tanggal
 * @property string $jam
 * @property string $naik_turun_harga
 * @property string $minimal_dp
 */
class Jadwal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jadwal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal, jam, naik_turun_harga, minimal_dp', 'required'),
			array('mobil', 'length', 'max'=>10),
			array('id_harga', 'length', 'max'=>18),
			array('supir', 'length', 'max'=>15),
			array('naik_turun_harga, minimal_dp', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jadwal, mobil, id_harga, supir, tanggal, jam, naik_turun_harga, minimal_dp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'price'=>array(self::BELONGS_TO,'HargaTujuan','id_harga'),
		'car'=>array(self::BELONGS_TO,'Mobil','mobil'),
		'driver'=>array(self::BELONGS_TO,'Supir','supir'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jadwal' => 'Id Jadwal',
			'mobil' => 'Mobil',
			'id_harga' => 'Id Harga',
			'supir' => 'Supir',
			'tanggal' => 'Tanggal',
			'jam' => 'Jam',
			'naik_turun_harga' => 'Naik Turun Harga',
			'minimal_dp' => 'Minimal Dp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jadwal',$this->id_jadwal);
		$criteria->compare('mobil',$this->mobil,true);
		$criteria->compare('id_harga',$this->id_harga,true);
		$criteria->compare('supir',$this->supir,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jam',$this->jam,true);
		$criteria->compare('naik_turun_harga',$this->naik_turun_harga,true);
		$criteria->compare('minimal_dp',$this->minimal_dp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jadwal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
