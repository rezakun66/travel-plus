<?php

/**
 * This is the model class for table "pencairan_token".
 *
 * The followings are the available columns in table 'pencairan_token':
 * @property string $waktu_pencairan
 * @property string $id_travel
 * @property string $jumlah_pencairan
 * @property string $waktu_tranfer
 * @property string $nomor_tranfer
 * @property string $bukti_tranfer
 * @property string $status
 */
class PencairanToken extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pencairan_token';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('waktu_pencairan, id_travel, jumlah_pencairan, waktu_tranfer, nomor_tranfer, bukti_tranfer, status', 'required'),
			array('id_travel', 'length', 'max'=>15),
			array('jumlah_pencairan', 'length', 'max'=>11),
			array('nomor_tranfer', 'length', 'max'=>20),
			array('bukti_tranfer', 'length', 'max'=>50),
			array('status', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('waktu_pencairan, id_travel, jumlah_pencairan, waktu_tranfer, nomor_tranfer, bukti_tranfer, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'waktu_pencairan' => 'Waktu Pencairan',
			'id_travel' => 'Id Travel',
			'jumlah_pencairan' => 'Jumlah Pencairan',
			'waktu_tranfer' => 'Waktu Tranfer',
			'nomor_tranfer' => 'Nomor Tranfer',
			'bukti_tranfer' => 'Bukti Tranfer',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('waktu_pencairan',$this->waktu_pencairan,true);
		$criteria->compare('id_travel',$this->id_travel,true);
		$criteria->compare('jumlah_pencairan',$this->jumlah_pencairan,true);
		$criteria->compare('waktu_tranfer',$this->waktu_tranfer,true);
		$criteria->compare('nomor_tranfer',$this->nomor_tranfer,true);
		$criteria->compare('bukti_tranfer',$this->bukti_tranfer,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PencairanToken the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
