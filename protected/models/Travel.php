<?php

/**
 * This is the model class for table "travel".
 *
 * The followings are the available columns in table 'travel':
 * @property string $id_travel
 * @property string $nama_travel
 * @property string $alamat
 * @property string $no_telp1
 * @property string $no_telp2
 * @property string $no_rekening
 * @property string $kode_bank
 * @property string $logo
 * @property string $saldo
 */
class Travel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'travel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_travel, nama_travel, alamat, no_telp1, no_telp2, no_rekening, kode_bank, saldo', 'required'),
			array('id_travel, no_telp1, no_telp2', 'length', 'max'=>15),
			array('nama_travel, logo', 'length', 'max'=>50),
			array('alamat', 'length', 'max'=>100),
			array('no_rekening', 'length', 'max'=>20),
			array('kode_bank', 'length', 'max'=>3),
			array('saldo', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_travel, nama_travel, alamat, no_telp1, no_telp2, no_rekening, kode_bank, logo, saldo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_travel' => 'Id Travel',
			'nama_travel' => 'Nama Travel',
			'alamat' => 'Alamat',
			'no_telp1' => 'No Telp1',
			'no_telp2' => 'No Telp2',
			'no_rekening' => 'No Rekening',
			'kode_bank' => 'Kode Bank',
			'logo' => 'Logo',
			'saldo' => 'Saldo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_travel',$this->id_travel,true);
		$criteria->compare('nama_travel',$this->nama_travel,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('no_telp1',$this->no_telp1,true);
		$criteria->compare('no_telp2',$this->no_telp2,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('kode_bank',$this->kode_bank,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('saldo',$this->saldo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Travel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
