<?php

/**
 * This is the model class for table "log_top_up".
 *
 * The followings are the available columns in table 'log_top_up':
 * @property integer $id_top_up
 * @property string $waktu_top_up
 * @property string $id_user
 * @property string $nomor_rekening
 * @property string $tanggal_transfer
 * @property string $jam_transfer
 * @property string $jumlah
 * @property string $bukti_transfer
 * @property string $status
 */
class LogTopUp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_top_up';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, nomor_rekening, tanggal_transfer, jam_transfer, jumlah, status', 'required'),
			array('id_user', 'length', 'max'=>15),
			array('nomor_rekening', 'length', 'max'=>20),
			array('jumlah', 'length', 'max'=>11),
			array('bukti_transfer', 'length', 'max'=>100),
			array('status', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_top_up, waktu_top_up, id_user, nomor_rekening, tanggal_transfer, jam_transfer, jumlah, bukti_transfer, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customer'=>array(self::BELONGS_TO,'Pelanggan','id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_top_up' => 'Id Top Up',
			'waktu_top_up' => 'Waktu Top Up',
			'id_user' => 'Id User',
			'nomor_rekening' => 'Nomor Rekening',
			'tanggal_transfer' => 'Tanggal Transfer',
			'jam_transfer' => 'Jam Transfer',
			'jumlah' => 'Jumlah',
			'bukti_transfer' => 'Bukti Transfer',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_top_up',$this->id_top_up);
		$criteria->compare('waktu_top_up',$this->waktu_top_up,true);
		$criteria->compare('id_user',$this->id_user,true);
		$criteria->compare('nomor_rekening',$this->nomor_rekening,true);
		$criteria->compare('tanggal_transfer',$this->tanggal_transfer,true);
		$criteria->compare('jam_transfer',$this->jam_transfer,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('bukti_transfer',$this->bukti_transfer,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function notif(){
		$model1=LogTopUp::model()->findAll(array("condition"=>"status = 'diproses'"));

		$value=0;
		foreach ($model1 as $key) {
			$value++;
		}
		return $value;	
	}
	public function uangMasuk($tgl,$tgl2){

		$timee=" 23:59:59";
		$timee0=" 00:00:00";
		$model1=LogTopUp::model()
		->findAll(array(     
        'condition'=>'status="diterima" and waktu_top_up between "'.$tgl.$timee0.'" and "'.$tgl2.$timee.'" '
                    ));
		$value=0;
		foreach ($model1 as $key) {
			$value=$value+$key->jumlah+10000;
		}
		return $value;	
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogTopUp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
