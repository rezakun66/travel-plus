<?php
/* @var $this KursiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kursis',
);

$this->menu=array(
	array('label'=>'Create Kursi', 'url'=>array('create')),
	array('label'=>'Manage Kursi', 'url'=>array('admin')),
);
?>

<h1>Kursis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
