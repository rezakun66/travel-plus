<?php
/* @var $this KursiController */
/* @var $data Kursi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kursi')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_kursi), array('view', 'id'=>$data->id_kursi)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pesan')); ?>:</b>
	<?php echo CHtml::encode($data->id_pesan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jadwal')); ?>:</b>
	<?php echo CHtml::encode($data->id_jadwal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kursi')); ?>:</b>
	<?php echo CHtml::encode($data->kursi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>