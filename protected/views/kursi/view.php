<?php
/* @var $this KursiController */
/* @var $model Kursi */

$this->breadcrumbs=array(
	'Kursis'=>array('index'),
	$model->id_kursi,
);

$this->menu=array(
	array('label'=>'List Kursi', 'url'=>array('index')),
	array('label'=>'Create Kursi', 'url'=>array('create')),
	array('label'=>'Update Kursi', 'url'=>array('update', 'id'=>$model->id_kursi)),
	array('label'=>'Delete Kursi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kursi),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kursi', 'url'=>array('admin')),
);
?>

<h1>View Kursi #<?php echo $model->id_kursi; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_kursi',
		'id_pesan',
		'id_jadwal',
		'kursi',
		'status',
	),
)); ?>
