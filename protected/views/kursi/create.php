<?php
/* @var $this KursiController */
/* @var $model Kursi */

$this->breadcrumbs=array(
	'Kursis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Kursi', 'url'=>array('index')),
	array('label'=>'Manage Kursi', 'url'=>array('admin')),
);
?>

<h1>Create Kursi</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>