<?php
/* @var $this KursiController */
/* @var $model Kursi */

$this->breadcrumbs=array(
	'Kursis'=>array('index'),
	$model->id_kursi=>array('view','id'=>$model->id_kursi),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kursi', 'url'=>array('index')),
	array('label'=>'Create Kursi', 'url'=>array('create')),
	array('label'=>'View Kursi', 'url'=>array('view', 'id'=>$model->id_kursi)),
	array('label'=>'Manage Kursi', 'url'=>array('admin')),
);
?>

<h1>Update Kursi <?php echo $model->id_kursi; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>