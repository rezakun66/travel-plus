<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Laporan</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Laporan</strong>
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
		</div>
</div>
			
<div class="wrapper wrapper-content animated fadeInRight">

        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1><?php
                $date= date_create($_GET['tgl']);
                $date2= date_create($_GET['tgl2']);
                 echo  date_format($date, 'j F Y').' - '.date_format($date2, 'j F Y'); ?></h1>
                
            </div>
        </div>

<form action="" method="get" class="form-inline">


<input class="form-control" name="r" placeholder=""  type="hidden" value="laporan/laporanTravel">

<div class="row">

		
		<div class="col-md-4">
		<label>Pilih Tanggal Awal</label>
		<input class=" form-control" placeholder="DD/MM/YYYY" name="tgl"  type="date">
		</div>
		<div class="col-md-4">
		<label>Pilih Tanggal Akhir</label>
		<input class=" form-control" placeholder="DD/MM/YYYY" name="tgl2" type="date">

		
		</div>
		<div class="col-md-2">
<button  class="btn btn-success btn-raised btn-flat" type="submit"><i class="fa fa-search"></i>Submit</button>
</div>

</div>	


</form>

<div class="row"> 
        	<div class="col-lg-12">
                <div class="widget style1 navy-bg">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <h2>Total Pendapatan</h2>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span>Saldo</span>
                                <h2 class="font-bold">Rp.<?php echo number_format($jumlahPembayaran['jumlah']); ?>-,</h2>
                            </div>
                        </div>
                </div>
            </div>
</div>

        <div class="row">
        	<div class="col-lg-6">
                <div class="widget style1 yellow-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa-3x">Rp</i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Jumlah Pengembalian Saldo </span>
                                <h2 class="font-bold"><?php echo number_format($jumlahRefund['jumlah']); ?>-,</h2>
                            </div>
                        </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="widget style1 lazur-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-ticket fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Pemesanan </span>
                            <h2 class="font-bold"><?php echo $jumlahPesan['jumlah'] ?></h2>
                        </div>
                    </div>
                </div>
            </div>
</div>
<div class="row">            
            <div class="col-lg-6">
                <div class="widget style1 red-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-ticket fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Pembatalan </span>
                            <h2 class="font-bold"><?php echo $jumlahBatal['jumlah'] ?></h2>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="col-lg-6">
                <div class="widget style1 yellow-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-car fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Trip Armada </span>
                            <h2 class="font-bold"><?php echo $jumlahArmada['jumlah'] ?></h2>
                        </div>
                    </div>
                </div>
            </div>
</div>





			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Pendapatan</h2>
					            </div>
					            <div class="ibox-content">

					           
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Waktu Pemesanan</th>
					                <th>Tujuan</th>
					                <th>Pelanggan</th>
					                <th>status</th>
					                <th>Pembayaran</th>

					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									
							    	$nomor=1;
							    	foreach($model2 as $data2){
							    		if($data2->schedule->car['id_travel']==$id){
										
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data2->waktu_pesan;?></td>
										<td><?php echo $data2->schedule->price->town['nama'];?></td>
										<td><?php echo $data2->customer['nama'];?></td>
										<td><?php echo $data2->status;?></td>
										<td>Rp. <?php echo number_format ($data2->pembayaran);?> -,</td>
										
										

					            	</tr>
					            	<?php
						    	$nomor++;
									}
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Pengembalian Saldo</h2>
					            </div>
					            <div class="ibox-content">

					           
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Waktu Pemesanan</th>
					                <th>Waktu Pembatalan</th>
					                <th>Pelanggan</th>
					                <th>Pengembalian Saldo</th>

					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									
							    	$nomor=1;
							    	foreach($model3 as $data3){
							    		if($data3->schedule->car['id_travel']==$id){
										
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data3->waktu_pesan;?></td>
						    			<td><?php echo $data3->waktu_batal;?></td>
										<td><?php echo $data3->customer['nama'];?></td>
										<td>Rp. <?php echo number_format ($data3->pembayaran);?> -,</td>
										
										

					            	</tr>
					            	<?php
						    	$nomor++;
									}
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>




			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Trip Armada</h2>
					            </div>
					            <div class="ibox-content">

					           
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Supir</th>
					                <th>Mobil</th>
					                <th>Kota</th>
					                <th>Harga</th>
					                <th>Waktu Berangkat</th>

					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									
							    	$nomor=1;
							    	foreach($model as $data){
										
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data->driver['nama'];?></td>
						    			<td><?php echo $data->mobil;?></td>
										<td><?php echo $data->price->town['nama'];?></td>
										<td>Rp. <?php echo number_format ($data->price['harga']+$data->naik_turun_harga);?> -,</td>
										<td><?php echo $data->tanggal.' '.$data->jam;?></td>
										

					            	</tr>
					            	<?php
						    	$nomor++;
									
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>




</div>