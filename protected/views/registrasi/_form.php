<?php
/* @var $this PelangganController */
/* @var $model Pelanggan */
/* @var $form CActiveForm */
?>



<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pelanggan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
                    <div class="ibox-title">

                        <div class="ibox-content">
						
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php // echo $form->labelEx($model,'id_pelanggan'); ?>
		<?php echo $form->hiddenField($model,'id_pelanggan',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','value'=>'08')); ?>
		<?php echo $form->error($model,'id_pelanggan'); ?>
	</div>
		<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model,'nama'); ?></label>
			<div class="col-sm-12">
		
		<?php echo $form->textField($model,'nama',array('size'=>50,'maxlength'=>50,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model,'jenis_kelamin'); ?></label>
			<br>
			<div class="col-sm-12">
				<?php echo $form->radioButtonList($model,'jenis_kelamin',array('L'=>'Laki-Laki','P'=>'Perempuan')); ?>
		<?php echo $form->error($model,'jenis_kelamin'); ?>
	</div>
	</div>
	</div>

		<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model,'alamat'); ?></label>
			<div class="col-sm-12">
		<?php echo $form->textArea($model,'alamat',array('size'=>60,'maxlength'=>100,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>
	</div>
	</div>
		<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model,'no_telp'); ?></label>
			<div class="col-sm-12">
		<?php echo $form->textField($model,'no_telp',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'no_telp'); ?>
	</div>
	</div>
	</div>
	
	<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model2,'password'); ?></label>
			<div class="col-sm-12">
		<?php echo $form->passwordField($model2,'password',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model2,'password'); ?>
	</div>
	</div>
	</div>

		<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model,'foto'); ?></label>
		<div class="col-sm-6">

		<?php echo $form->fileField($model,'foto'); ?>
		<?php echo $form->error($model,'foto'); ?>
	

	</div>
	</div>
	</div>

		<div class="row">
		<?php echo $form->labelEx($model,''); ?>
		<?php echo $form->hiddenField($model,'saldo',array('value'=>0)); ?>
		<?php echo $form->error($model,'saldo'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrasi' : 'Save',array(
			'class'=>'btn btn-block btn-outline btn-primary ')); ?>
			<?php if ($model->isNewRecord) {
		echo CHtml::resetButton('Reset',array('class'=>'btn btn-block btn-outline btn-warning')); }?>
	</div>

<?php $this->endWidget(); ?>

</div>
</div>
</div>
</div>
</div><!-- form -->
</div><!-- form -->