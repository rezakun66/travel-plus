<?php
/* @var $this HargaTujuanController */
/* @var $model HargaTujuan */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Data Harga Tujuan</h2>
		            </div>
		            <div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'harga-tujuan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
	'enctype' => 'multipart/form-data',
		),	
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


		<?php echo $form->hiddenField($model,'id_harga',array('size'=>18,'maxlength'=>18,'value'=>'test')); ?>
		<?php echo $form->error($model,'id_harga'); ?>


		<?php echo $form->hiddenField($model,'id_travel',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','value'=>Yii::app()->user->getId())); ?>
		<?php echo $form->error($model,'id_travel'); ?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_kota'); ?> </label>
		
		<div class="col-md-6">
			<select name="idKota" data-placeholder="Pencarian Kota..." class="select2_demo_3 form-control m-b"  tabindex="2">
			<option></option>
			<?php
					$town = Kota::model()->findAll();
					foreach($town as $p)
					{
						echo '<option value="'.$p->id_kota.'">'.$p->nama.'</option>';
					}
				?>
			</select>
			</div>
			</div>
			</div>
			<br/>


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'harga'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'harga',array('size'=>11,'maxlength'=>11,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'harga'); ?>
	</div>
	</div>
	</div>


	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->