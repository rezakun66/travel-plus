<?php
/* @var $this HargaTujuanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Harga Tujuans',
);

$this->menu=array(
	array('label'=>'Create HargaTujuan', 'url'=>array('create')),
	array('label'=>'Manage HargaTujuan', 'url'=>array('admin')),
);
?>

<h1>Harga Tujuans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
