<?php
/* @var $this HargaTujuanController */
/* @var $model HargaTujuan */

$this->breadcrumbs=array(
	'Harga Tujuans'=>array('index'),
	$model->id_harga=>array('view','id'=>$model->id_harga),
	'Update',
);

$this->menu=array(
	array('label'=>'List HargaTujuan', 'url'=>array('index')),
	array('label'=>'Create HargaTujuan', 'url'=>array('create')),
	array('label'=>'View HargaTujuan', 'url'=>array('view', 'id'=>$model->id_harga)),
	array('label'=>'Manage HargaTujuan', 'url'=>array('admin')),
);
?>

<h1>Update HargaTujuan <?php echo $model->id_harga; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>