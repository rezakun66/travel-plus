<?php
/* @var $this HargaTujuanController */
/* @var $data HargaTujuan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_harga')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_harga), array('view', 'id'=>$data->id_harga)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_travel')); ?>:</b>
	<?php echo CHtml::encode($data->id_travel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kota')); ?>:</b>
	<?php echo CHtml::encode($data->id_kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />


</div>