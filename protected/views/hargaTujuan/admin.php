<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Harga Tujuan</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Harga Tujuan</strong>
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
				
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Harga Tujuan</h2>
					            </div>
					            <div class="ibox-content">

					            <a href="index.php?r=HargaTujuan/create"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-plus-square"></i>Tambah Data</button></a>
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>id harga</th>
					                <th>Kota</th>
					                <th>Harga</th>

					                <th>action</th>
					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
							    	$nomor=1;
							    	foreach($model as $data){
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data->id_harga;?></td>
						    			<td><?php echo $data->id_kota;?></td>
										<td>Rp. <?php echo number_format ($data->harga);?> -,</td>
									<td width="130" class="text-right">
									<div class="tooltip-demo">
                                        <div class="btn-group">
                                            <a href="index.php?r=hargaTujuan/view&id=<?php echo $data->id_harga;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Lihat Detail" data-original-title="Tooltip on left" >Lihat</button></a>
                                            <a href="index.php?r=hargaTujuan/update&id=<?php echo $data->id_harga;?>"> <button class="btn-warning btn btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Data" data-original-title="Tooltip on top" >Edit</button></a>
                                            <a href="index.php?r=hargaTujuan/delete&id=<?php echo $data->id_harga;?>"> <button class="btn-danger btn btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus Data" data-original-title="Tooltip on right" >Hapus</button></a>
                                        </div>
                                        </div>
                                    </td>

					            	</tr>
					            	<?php
						    	$nomor++;
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>






</div>
</div>