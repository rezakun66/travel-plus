<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Harga Tujuan</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Harga Tujuan</strong>
					</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data Harga Tujuan</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								<table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>id harga</th>
					                <td><?php echo $model->id_harga;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>kota</th>
					                <td><?php echo $model->id_kota;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>harga</th>
					                <td>Rp. <?php echo number_format ($model->harga);?> -,</td>
					            </tr>
					            


						    	</table>
								<a href="index.php?r=HargaTujuan/admin"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-long-arrow-right"></i> NEXT</button></a>
								
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>