<?php
/* @var $this MobilController */
/* @var $model Mobil */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Data Harga Tujuan</h2>
		            </div>
		            <div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mobil-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'flat_mobil'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'flat_mobil',array('size'=>10,'maxlength'=>10,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'flat_mobil'); ?>
	</div>
	</div>
	</div>


		<?php echo $form->hiddenField($model,'id_travel',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','value'=>Yii::app()->user->getId())); ?>
		<?php echo $form->error($model,'id_travel'); ?>

			<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'jenis_mobil'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'jenis_mobil',array('size'=>30,'maxlength'=>30,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'jenis_mobil'); ?>
	</div>
	</div>
	</div>
		
	
	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->