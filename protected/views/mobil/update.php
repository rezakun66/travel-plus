<?php
/* @var $this MobilController */
/* @var $model Mobil */

$this->breadcrumbs=array(
	'Mobils'=>array('index'),
	$model->flat_mobil=>array('view','id'=>$model->flat_mobil),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mobil', 'url'=>array('index')),
	array('label'=>'Create Mobil', 'url'=>array('create')),
	array('label'=>'View Mobil', 'url'=>array('view', 'id'=>$model->flat_mobil)),
	array('label'=>'Manage Mobil', 'url'=>array('admin')),
);
?>

<h1>Update Mobil <?php echo $model->flat_mobil; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>