<?php
/* @var $this MobilController */
/* @var $data Mobil */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('flat_mobil')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->flat_mobil), array('view', 'id'=>$data->flat_mobil)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_travel')); ?>:</b>
	<?php echo CHtml::encode($data->id_travel); ?>
	<br />


</div>