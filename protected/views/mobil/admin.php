<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Mobil</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Mobil</strong>
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
				
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Mobil</h2>
					            </div>
					            <div class="ibox-content">

					            <a href="index.php?r=mobil/create"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-plus-square"></i>Tambah Data</button></a>
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>flat nomor mobil</th>
					                <th>jenis mobil</th>


					                <th>action</th>
					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
							    	$nomor=1;
							    	foreach($model as $data){
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>

										<td><?php echo $data->flat_mobil;?></td>
										<td><?php echo $data->jenis_mobil;?></td>
									<td width="130" class="text-right">
									<div class="tooltip-demo">
                                        <div class="btn-group">
                                            <a href="index.php?r=mobil/view&id=<?php echo $data->flat_mobil;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Lihat Detail" data-original-title="Tooltip on left" >Lihat</button></a>
                                            <a href="index.php?r=mobil/update&id=<?php echo $data->flat_mobil;?>"> <button class="btn-warning btn btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Data" data-original-title="Tooltip on top" >Edit</button></a>
                                            <a href="index.php?r=mobil/delete&id=<?php echo $data->flat_mobil;?>"> <button class="btn-danger btn btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus Data" data-original-title="Tooltip on right" >Hapus</button></a>
                                        </div>
                                        </div>
                                    </td>

					            	</tr>
					            	<?php
						    	$nomor++;
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>






</div>
</div>