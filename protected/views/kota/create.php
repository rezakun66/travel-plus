<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Kota</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Data master</strong>
					</li>
						<li>
								<strong>Kota</strong>
						</li>
						<li>
								<strong>Tambah Data</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">

				</div>
		</div>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>