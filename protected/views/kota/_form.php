<?php
/* @var $this KotaController */
/* @var $model Kota */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Data Kota</h2>
		            </div>
		            <div class="ibox-content">



<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kota-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_kota'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'id_kota',array('size'=>3,'maxlength'=>3,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'id_kota'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'nama'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'nama',array('size'=>30,'maxlength'=>30,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'foto'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->fileField($model,'foto'); ?>
		<?php echo $form->error($model,'foto'); ?>
	</div>
	</div>
	</div>
<br/>
	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->	
</div><!-- form -->