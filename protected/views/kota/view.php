<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Kota</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Data master</strong>
					</li>
						<li>
								<strong>Kota</strong>
						</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">

				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data Kota</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								<p align="center" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/kota/<?php echo $model->foto;?>" width="200" height="200" > </p>
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>id kota</th>
					                <td><?php echo $model->id_kota;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nama</th>
					                <td><?php echo $model->nama;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>foto</th>
					                <td><?php echo $model->foto;?></td>
					            </tr>
					            


						    	</table>
								<a href="index.php?r=Kota/admin"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-long-arrow-right"></i> NEXT</button></a>
								
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>