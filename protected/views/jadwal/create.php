<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Penjadwalan</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Penjadwalan</strong>
					</li>
					<li>
							Tambah Data
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
				
		</div>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>