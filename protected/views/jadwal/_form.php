<?php
/* @var $this JadwalController */
/* @var $model Jadwal */
/* @var $form CActiveForm */
$id=Yii::app()->session->get('id');
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Data Jadwal</h2>
		            </div>
		            <div class="ibox-content">
<form action="" method="get">


<input class="form-control" name="r" placeholder=""  type="hidden" value="jadwal/create">

<div class="row">


		<div class="col-md-5">
		
		<input class=" form-control" placeholder="DD/MM/YYYY" name="tgl" type="date">
		
		</div>
			



</div>	
<br>
<button  class="btn btn-succes btn-raised btn-flat" type="submit"><i class="fa fa-search"></i>Submit</button>
</form>					
					
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jadwal-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php 
	$datee = Yii::app()->request->getParam('tgl');
	 
	echo $form->errorSummary($model); ?>

		<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'mobil'); ?> </label>
		<div class="col-sm-6">		
			<select name="cars" data-placeholder="pilih mobil" class="select2_demo_3 form-control m-b"  tabindex="2">
			<option></option>
			<?php
					$cars = Mobil::model()->findAll(array('condition'=>'id_travel="'.$id.'"'));
					$jadw = Jadwal::model()->findAll(array('condition'=>'tanggal="'.$datee.'"'));
					foreach($cars as $p)
					{
						$platuk=true;
						foreach($jadw as $data2){
						  if($p->flat_mobil==$data2->mobil){
							$platuk=false;
							}
						}
						
						if($platuk==true){
							echo '<option value="'.$p->flat_mobil.'">'.$p->flat_mobil.'</option>';
						}
					}
				?>
			</select>
			</div>
			</div>
			</div>
			<br/>
			
		<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo "Tujuan *"; ?> </label>
		<div class="col-sm-6">		
			<select name="tujuan" data-placeholder="pilih tujuan" class="select2_demo_3 form-control m-b"  tabindex="2">
			<option></option>
			<?php
					
					$price = HargaTujuan::model()->findAll(array('condition'=>'id_travel="'.$id.'"'));
					
					foreach($price as $p)
					{
						echo '<option value="'.$p->id_harga.'">'.$p->town['nama'].'-> Rp. '.number_format($p->harga).' -, </option>';
					}
				?>
			</select>
			</div>
			</div>
			</div>
			<br/>			
		<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'Supir'); ?> </label>
		<div class="col-sm-6">		
			<select name="driver" data-placeholder="pilih supir" class="select2_demo_3 form-control m-b"  tabindex="2">
			<option></option>
			<?php
					$driver = Supir::model()->findAll(array('condition'=>'id_travel="'.$id.'"'));
					foreach($driver as $p)
					{
						$platuk=true;
						foreach($jadw as $data2){
						  if($p->id_supir==$data2->supir){
							$platuk=false;
							}
						}
						if($platuk){
						echo '<option value="'.$p->id_supir.'">'.$p->id_supir.' -> '.$p->nama.' </option>';
						}
					}
				?>
			</select>
			</div>
			</div>
			</div>
			<br/>
			





	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'tanggal'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'tanggal',array('class'=>'form-control m-b','value'=>$datee,'readonly'=>true)); ?>
		<?php echo $form->error($model,'tanggal'); ?>
	</div>
	</div>
	</div>
	
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'jam'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->timeField($model,'jam',array('class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'jam'); ?>
	</div>
	</div>
	</div>
	


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'naik_turun_harga'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'naik_turun_harga',array('size'=>11,'maxlength'=>11,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'naik_turun_harga'); ?>
	</div>
	</div>
	</div>




	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>


<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->


</div><!-- form -->

<div class="row">
    	
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Jadwal Tanggal <?php echo date('d-m-Y'); ?></h2>
		            </div>
		            <div class="ibox-content">
					
					<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>Id jadwal</th>
					                <th>Mobil</th>
					                <th>Kota</th>
					                <th>Harga</th>
					                <th>Waktu</th>
					                <th>Diskon</th>

					                <th>action</th>
					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									$datee=Yii::app()->request->getParam('tgl');
									$modelJdwl=Jadwal::model()->findAll(array('condition'=>'tanggal = "'.$datee.'"'));
									
									$id=Yii::app()->session->get('id');
									//$modelJdwl=Jadwal::model()->findAll();
							    	foreach($modelJdwl as $data){
										if($data->price['id_travel']==$id){
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $data->id_jadwal;?></td>
						    			<td><?php echo $data->mobil;?></td>
										<td><?php echo $data->price->town['nama'];?></td>
										<td>Rp. <?php echo number_format ($data->price['harga']);?> -,</td>
										<td><?php echo $data->tanggal.' '.$data->jam;?></td>
										<td><?php echo $data->naik_turun_harga;?></td>
									<td>
									<div class="tooltip-demo">
                                        <div class="btn-group">
                                            <a href="index.php?r=jadwal/delete2&id=<?php echo $data->id_jadwal;?>&tgl=<?php echo $datee; ?>"> <button class="btn-danger btn btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus Data" data-original-title="Tooltip on right" >Hapus</button></a>
                                        </div>
                                        </div>
                                    </td>

					            	</tr>
					            	<?php
									}
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->


</div><!-- form -->
</div><!-- form -->
</div><!-- form -->