<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Jadwal</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Jadwal</strong>
					</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data Jadwal</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>Id jadwal</th>
					                <td><?php echo $model->id_jadwal;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Mobil</th>
					                <td><?php echo $model->mobil;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Supir</th>
					                <td><?php echo $model->driver['nama']." -> ".$model->supir;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Kota</th>
					                <td><?php echo $model->price->town['nama'];?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Harga</th>
					                <td>Rp. <?php echo number_format ($model->price['harga']);?> -,</td>
					            </tr>
					            <tr class="gradeX">
					                <th>Tanggal</th>
					                <td><?php  echo $model->tanggal; ?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Jam</th>
					                <td><?php  echo $model->jam; ?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Diskon</th>
					                <td><?php
										$tot=$model->price['harga']+$model->naik_turun_harga;
										$disc1=$model->price['harga']-$tot;
										$disc2=$disc1/$model->price['harga'];
										$disc3=$disc2*100;
										if($disc3>0){
										echo $disc3;
										}else{
											echo "0";
										}
										
									?>%</td>
					            </tr>
					            


						    	</table>
								<a href="index.php?r=jadwal/admin"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-long-arrow-right"></i> NEXT</button></a>
								
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>