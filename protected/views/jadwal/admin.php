<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Penjadwalan</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Penjadwalan</strong>
					</li>
				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
				
		</div>
</div>


<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Jadwal</h2>
					            </div>
					            <div class="ibox-content">

					            <a href="index.php?r=jadwal/create&tgl=<?php echo date("Y-m-d"); ?>"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-plus-square"></i>Tambah Data</button></a>
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Id jadwal</th>
					                <th>Mobil</th>
					                <th>Kota</th>
					                <th>Harga</th>
					                <th>Waktu</th>
					                <th>Diskon</th>

					                <th>action</th>
					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									
							    	$nomor=1;
							    	foreach($model as $data){
										if($data->price['id_travel']==$id){
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data->id_jadwal;?></td>
						    			<td><?php echo $data->mobil;?></td>
										<td><?php echo $data->price->town['nama'];?></td>
										<td>Rp. <?php echo number_format ($data->price['harga']);?> -,</td>
										<td><?php echo $data->tanggal.' '.$data->jam;?></td>
										<td><?php echo $data->naik_turun_harga;?></td>
									<td width="135" class="text-right">
									<div class="tooltip-demo">
                                        <div class="btn-group">
                                            <a href="index.php?r=jadwal/view&id=<?php echo $data->id_jadwal;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Lihat Detail" data-original-title="Tooltip on left" >Lihat</button></a>
                                            <a href="index.php?r=jadwal/update&id=<?php echo $data->id_jadwal;?>"> <button class="btn-warning btn btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Data" data-original-title="Tooltip on top" >Edit</button></a>
                                            <a href="index.php?r=jadwal/delete&id=<?php echo $data->id_jadwal;?>"> <button class="btn-danger btn btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus Data" data-original-title="Tooltip on right" >Hapus</button></a>
                                        </div>
                                        </div>
                                    </td>

					            	</tr>
					            	<?php
						    	$nomor++;
									}
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>


</div>
</div>