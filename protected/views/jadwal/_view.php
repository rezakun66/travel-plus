<?php
/* @var $this JadwalController */
/* @var $data Jadwal */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jadwal')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_jadwal), array('view', 'id'=>$data->id_jadwal)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobil')); ?>:</b>
	<?php echo CHtml::encode($data->mobil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_harga')); ?>:</b>
	<?php echo CHtml::encode($data->id_harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supir')); ?>:</b>
	<?php echo CHtml::encode($data->supir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu')); ?>:</b>
	<?php echo CHtml::encode($data->waktu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('naik_turun_harga')); ?>:</b>
	<?php echo CHtml::encode($data->naik_turun_harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('minimal_dp')); ?>:</b>
	<?php echo CHtml::encode($data->minimal_dp); ?>
	<br />


</div>