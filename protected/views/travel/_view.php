<?php
/* @var $this TravelController */
/* @var $data Travel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_travel')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_travel), array('view', 'id'=>$data->id_travel)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_travel')); ?>:</b>
	<?php echo CHtml::encode($data->nama_travel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_telp1')); ?>:</b>
	<?php echo CHtml::encode($data->no_telp1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_telp2')); ?>:</b>
	<?php echo CHtml::encode($data->no_telp2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_rekening')); ?>:</b>
	<?php echo CHtml::encode($data->no_rekening); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_bank')); ?>:</b>
	<?php echo CHtml::encode($data->kode_bank); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('token')); ?>:</b>
	<?php echo CHtml::encode($data->token); ?>
	<br />

	*/ ?>

</div>