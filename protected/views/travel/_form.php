<?php
/* @var $this TravelController */
/* @var $model Travel */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Data Travel</h2>
		            </div>
		            <div class="ibox-content">
					
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'travel-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),	
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_travel'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'id_travel',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'id_travel'); ?>
	</div>
	</div>
	</div>
	
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'nama_travel'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'nama_travel',array('size'=>50,'maxlength'=>50,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'nama_travel'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'alamat'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textArea($model,'alamat',array('size'=>60,'maxlength'=>100,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'no_telp1'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'no_telp1',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'no_telp1'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'no_telp2'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'no_telp2',array('class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'no_telp2'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'no_rekening'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'no_rekening',array('size'=>20,'maxlength'=>20,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'no_rekening'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_pelanggan'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'kode_bank',array('size'=>3,'maxlength'=>3,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'kode_bank'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'token'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'saldo',array('size'=>11,'maxlength'=>11,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'saldo'); ?>
	</div>
	</div>
	</div>

		<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'logo'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->fileField($model,'logo'); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>
	</div>
	</div>
	
	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->