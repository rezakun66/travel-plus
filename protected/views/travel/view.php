<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Travel</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Data master</strong>
					</li>
						<li>
								<strong>Travel</strong>
						</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">

				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data Travel</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								<p align="center" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/travel/<?php echo $model->logo;?>" width="200" height="200" > </p>
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>id travel</th>
					                <td><?php echo $model->id_travel;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nama travel</th>
					                <td><?php echo $model->nama_travel;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>alamat</th>
					                <td><?php echo $model->alamat;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nomor telepon 1</th>
					                <td><?php echo $model->no_telp1;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nomor telepon 2</th>
					                <td><?php echo $model->no_telp2;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nomor rekening</th>
					                <td><?php echo $model->no_rekening;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>kode bank</th>
					                <td><?php echo $model->kode_bank;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>logo</th>
					                <td><?php echo $model->logo;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>saldo</th>
					                <td><?php echo $model->saldo;?></td>
					            </tr>
					            


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>