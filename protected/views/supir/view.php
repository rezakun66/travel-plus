<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Supir</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Supir</strong>
					</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo $modelTra->saldo; ?> -,</strong>
				<?php } ?>
				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data Supir</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								<p align="center" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/supir/<?php echo $model->foto;?>" width="200" height="200" > </p>
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>id supir</th>
					                <td><?php echo $model->id_supir;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nama</th>
					                <td><?php echo $model->nama;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>jenis kelamin</th>
					                <td><?php echo $model->jenis_kelamin;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>alamat</th>
					                <td><?php echo $model->alamat;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nomor telepon</th>
					                <td><?php echo $model->no_telp;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>foto</th>
					                <td><?php echo $model->foto;?></td>
					            </tr>


						    	</table>
								<a href="index.php?r=supir/admin"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-long-arrow-right"></i> NEXT</button></a>
								
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>