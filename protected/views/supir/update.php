<?php
/* @var $this SupirController */
/* @var $model Supir */

$this->breadcrumbs=array(
	'Supirs'=>array('index'),
	$model->id_supir=>array('view','id'=>$model->id_supir),
	'Update',
);

$this->menu=array(
	array('label'=>'List Supir', 'url'=>array('index')),
	array('label'=>'Create Supir', 'url'=>array('create')),
	array('label'=>'View Supir', 'url'=>array('view', 'id'=>$model->id_supir)),
	array('label'=>'Manage Supir', 'url'=>array('admin')),
);
?>

<h1>Update Supir <?php echo $model->id_supir; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>