<?php
/* @var $this SupirController */
/* @var $data Supir */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_supir')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_supir), array('view', 'id'=>$data->id_supir)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_travel')); ?>:</b>
	<?php echo CHtml::encode($data->id_travel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_telp')); ?>:</b>
	<?php echo CHtml::encode($data->no_telp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foto')); ?>:</b>
	<?php echo CHtml::encode($data->foto); ?>
	<br />


</div>