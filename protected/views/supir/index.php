<?php
/* @var $this SupirController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Supirs',
);

$this->menu=array(
	array('label'=>'Create Supir', 'url'=>array('create')),
	array('label'=>'Manage Supir', 'url'=>array('admin')),
);
?>

<h1>Supirs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
