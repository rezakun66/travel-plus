<?php
/* @var $this SupirController */
/* @var $model Supir */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Data Supir</h2>
		            </div>
		            <div class="ibox-content">
					
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supir-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
	'enctype' => 'multipart/form-data',
		),	
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>




		<?php echo $form->hiddenField($model,'id_travel',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','value'=>Yii::app()->user->getId())); ?>
		<?php echo $form->error($model,'id_travel'); ?>
		
		<?php echo $form->hiddenField($model2,'id_user',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','value'=>'0987')); ?>
		<?php echo $form->error($model2,'id_user'); ?>


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'nama'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'nama',array('size'=>50,'maxlength'=>50,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'jenis_kelamin'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->radioButtonList($model,'jenis_kelamin',array('L'=>'Laki-Laki','P'=>'Perempuan')); ?>
		<?php echo $form->error($model,'jenis_kelamin'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'alamat'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textArea($model,'alamat',array('size'=>60,'maxlength'=>100,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'no_telp'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'id_supir',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'id_supir'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'foto'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->fileField($model,'foto'); ?>
		<?php echo $form->error($model,'foto'); ?>
	</div>
	</div>
	</div>
	
	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->