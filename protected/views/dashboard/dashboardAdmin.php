<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Dashboard</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Dashboard</strong>
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
		</div>
</div>
			
<div class="wrapper wrapper-content animated fadeInRight">

        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1><?php echo date('d F Y');?></h1>
                
            </div>
        </div>	


        <div class="row">
        	            <div class="col-lg-6">
                <div class="widget style1 navy-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa-3x">Rp</i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Jumlah Uang Masuk </span>
                                <h2 class="font-bold"><?php
                                $datee=date('Y-m-d'); 
                                $wangMasuk=LogTopUp::model()->uangMasuk($datee,$datee);
                                echo number_format($wangMasuk); ?>-,</h2>
                            </div>
                        </div>
                </div>
            </div>
			
            <div class="col-lg-6">
                <div class="widget style1 red-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa-3x">Rp</i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Uang Keluar </span>
                            <h2 class="font-bold"><?php
                            $wangKeluar=PencairanSaldo::model()->uangKeluar($datee,$datee);
                             echo number_format($wangKeluar); ?>-,</h2>
                        </div>
                    </div>
                </div>
            </div>
			
			</div>
		<div class="row">
            <div class="col-lg-6">
                <div class="widget style1 lazur-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-btc fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Top Up </span>
                            <h2 class="font-bold"><?php echo $jumlahTopUp['jumlah']; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="widget style1 yellow-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-dollar fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Pencairan </span>
                            <h2 class="font-bold"><?php echo $jumlahCair['jumlah']; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
	</div>


<div class="row"> 
        	<div class="col-lg-12">
                <div class="widget style1 navy-bg">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <h2>Total Pendapatan</h2>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span>Saldo</span>
                                <h2 class="font-bold">Rp.<?php 
                                $totall=$wangMasuk-$wangKeluar;
                                echo number_format($totall); ?>-,</h2>
                            </div>
                        </div>
                </div>
            </div>
</div>




			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Top Up Hari Ini</h2>
					            </div>
					            <div class="ibox-content">

					           
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Waktu Top Up</th>
					                <th>Waktu Transfer</th>
					                <th>Pelanggan</th>
					                <th>Jumlah Uang Masuk</th>

					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									
							    	$nomor=1;
							    	foreach($model as $data){
										
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data->waktu_top_up;?></td>
						    			<td><?php echo $data->tanggal_transfer.' '.$data->jam_transfer;?></td>
						    			<td><?php echo $data->customer['nama'];?></td>
										<td>Rp. <?php echo number_format ($data->jumlah+10000);?> -,</td>
										

					            	</tr>
					            	<?php
						    	$nomor++;
									
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Pencairan Hari Ini</h2>
					            </div>
					            <div class="ibox-content">

					           
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Waktu Selesai Pencairan</th>
					                <th>Waktu Transfer</th>
					                <th>Travel</th>
					                <th>Jumlah Uang Keluar</th>

					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									
							    	$nomor=1;
							    	foreach($model2 as $data2){
										
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data2->waktu_selesai ;?></td>
						    			<td><?php echo $data2->tanggal_transfer.' '.$data2->jam_transfer;?></td>
						    			<td><?php echo $data2->travel['nama_travel'];?></td>
										<td>Rp. <?php echo number_format ($data2->jumlah_pencairan-10000+6500);?> -,</td>
										

					            	</tr>
					            	<?php
						    	$nomor++;
									
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>