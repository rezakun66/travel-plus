<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Dashboard</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Dashboard</strong>
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
		</div>
</div>
			
<div class="wrapper wrapper-content animated fadeInRight">

        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1><?php echo date('d F Y');?></h1>
                
            </div>
        </div>	
        <div class="row">
        	            <div class="col-lg-3">
                <div class="widget style1 navy-bg">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <i class="fa-3x">Rp</i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Jumlah Pendapatan </span>
                                <h2 class="font-bold"><?php echo number_format($jumlahPembayaran['jumlah']); ?>-,</h2>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 lazur-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-ticket fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Pemesanan </span>
                            <h2 class="font-bold"><?php echo $jumlahPesan['jumlah'] ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 red-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-ticket fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Jumlah Pembatalan </span>
                            <h2 class="font-bold"><?php echo $jumlahBatal['jumlah'] ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 yellow-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-car fa-4x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Armada Berangkat </span>
                            <h2 class="font-bold"><?php echo $jumlahArmada['jumlah'] ?></h2>
                        </div>
                    </div>
                </div>
            </div>
</div>



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Data Jadwal Hari Ini</h2>
					            </div>
					            <div class="ibox-content">

					           
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Supir</th>
					                <th>Mobil</th>
					                <th>Kota</th>
					                <th>Harga</th>
					                <th>Jam</th>

					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
									
							    	$nomor=1;
							    	foreach($model as $data){
										
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data->driver['nama'];?></td>
						    			<td><?php echo $data->mobil;?></td>
										<td><?php echo $data->price->town['nama'];?></td>
										<td>Rp. <?php echo number_format ($data->price['harga']+$data->naik_turun_harga);?> -,</td>
										<td><?php echo $data->jam;?></td>
										

					            	</tr>
					            	<?php
						    	$nomor++;
									
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>