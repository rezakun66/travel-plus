<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Pelanggan</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Data master</strong>
					</li>
						<li>
								<strong>Pelanggan</strong>
						</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">

				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data Pelanggan</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								<p align="center" ><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/pelanggan/<?php echo $model->foto;?>" width="200" height="200" > </p>
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>id pelanggan</th>
					                <td><?php echo $model->id_pelanggan;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nama</th>
					                <td><?php echo $model->nama;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>jenis kelamin</th>
					                <td><?php echo $model->jenis_kelamin;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>alamat</th>
					                <td><?php echo $model->alamat;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nomor telepon</th>
					                <td><?php echo $model->no_telp;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Saldo</th>
					                <td><?php echo $model->saldo;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>foto</th>
					                <td><?php echo $model->foto;?></td>
					            </tr>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>