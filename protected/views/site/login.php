<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h2>Log in</h2>



<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
                    <div class="ibox-title">

                        <div class="ibox-content">
						
	<p class="note">Fields with <span class="required">*</span> are required.</p>


		<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model,'username'); ?></label>
			<div class="col-sm-12">
		<?php echo $form->textField($model,'username',array('class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	</div>
	</div>


		<div class="row">
		<div class="form-group"><label class="col-sm-12 control-label"><?php echo $form->labelEx($model,'password'); ?></label>
			<div class="col-sm-12">
		<?php echo $form->passwordField($model,'password',array('class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'password'); ?>

	</div>
	</div>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Masuk',array('class'=>'btn btn-block btn-outline btn-primary')); ?>
	</div>
		<div class="row rememberMe">
	Belum memiliki akun? daftar <a href="index.php?r=registrasi">di sini</a>	
		</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
