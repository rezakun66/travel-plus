<?php
/* @var $this LogTopUpController */
/* @var $model LogTopUp */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Validasi Top Up</h2>
		            </div>
		            <div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'log-top-up-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),	
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>




	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_user'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'id_user',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','readonly'=>true )); ?>
		<?php echo $form->error($model,'id_user'); ?>
</div>
</div>
</div>
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> Nama Pelanggan </label>
		 	<div class="col-sm-6">
		 		<input type="text" class="form-control m-b" readonly="true" value="<?php echo $model->customer['nama']; ?>" >
</div>
</div>
</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'nomor_rekening'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'nomor_rekening',array('class'=>'form-control m-b','size'=>15,'maxlength'=>15,'readonly'=>true)); ?>
		<?php echo $form->error($model,'nomor_rekening'); ?>
	</div>
	</div>
	</div>
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'tanggal_transfer'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->dateField($model,'tanggal_transfer',array('class'=>'form-control m-b','readonly'=>true)); ?>
		<?php echo $form->error($model,'tanggal_transfer'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'jam_transfer'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->timeField($model,'jam_transfer',array('class'=>'form-control m-b','readonly'=>true)); ?>
		<?php echo $form->error($model,'jam_transfer'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> Jumlah  </label>
		 	<div class="col-sm-6">
			<input type="text" class="form-control m-b" readonly="true" value="Rp. <?php echo number_format($model->jumlah); ?> -," >
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'bukti_transfer'); ?> </label>
		 	<div class="col-sm-6">
		 	<a href="<?php echo Yii::app()->request->baseUrl; ?>/images/topup/<?php echo $model->bukti_transfer; ?>">	
			<img alt="image" width="205" height="305" src="<?php echo Yii::app()->request->baseUrl; ?>/images/topup/<?php echo $model->bukti_transfer; ?>" />
			</a>
	</div>
	</div>
	</div>
<br>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'status'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->radioButtonList($model,'status',array('ditolak'=>'Tolak','diterima'=>'Terima')); ?>
		<?php echo $form->error($model,'status'); ?>
</div>
</div>
</div>

	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Simpan', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->