<?php
/* @var $this LogTopUpController */
/* @var $model LogTopUp */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Top Up</h2>
		            </div>
		            <div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'log-top-up-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),	
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>





		<?php echo $form->hiddenField($model,'id_user',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','value'=>Yii::app()->session->get('id') )); ?>
		<?php echo $form->error($model,'id_user'); ?>


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'nomor_rekening'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'nomor_rekening',array('class'=>'form-control m-b','size'=>15,'maxlength'=>15,)); ?>
		<?php echo $form->error($model,'nomor_rekening'); ?>
	</div>
	</div>
	</div>
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'tanggal_transfer'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->dateField($model,'tanggal_transfer',array('class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'tanggal_transfer'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'jam_transfer'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->timeField($model,'jam_transfer',array('class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'jam_transfer'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'jumlah'); ?> </label>
		 	<div class="col-sm-6">
		
					<?php echo $form->dropDownList($model,'jumlah',array(
				'90000'=> 'Rp.100.000-, = 90.000 saldo',
				'190000'=> 'Rp.200.000-, = 190.000 saldo',
				'290000'=> 'Rp.300.000-, = 290.000 saldo',
				'390000'=> 'Rp.400.000-, = 390.000 saldo',
				'490000'=> 'Rp.500.000-, = 490.000 saldo',
				'990000'=> 'Rp.1.000.000-, = 990.000 saldo',
				
			),
				array('empty'=>'','class'=>'form-control m-b ','required'=>'" "')); ?>
		<?php echo $form->error($model,'jumlah'); ?>
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'bukti_transfer'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->fileField($model,'bukti_transfer'); ?>
		<?php echo $form->error($model,'bukti_transfer'); ?>
	</div>
	</div>
	</div>


		<?php echo $form->hiddenField($model,'status',array('size'=>8,'maxlength'=>8,'class'=>'form-control m-b','value'=>'diproses')); ?>
		<?php echo $form->error($model,'status'); ?>


	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->