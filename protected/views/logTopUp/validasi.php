<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Top Up</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Validasi</strong>
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
					
				<strong>Saldo : Rp.<?php echo $modelTra->saldo; ?> -,</strong>
				<?php } ?>
				</div>
				
		</div>
</div>

<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Validasi Top Up</h2>
					            </div>
					            <div class="ibox-content">

					            <div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Waktu top up</th>
					                <th>Tanggal Transfer</th>
					                <th>Jam Transfer</th>
					                <th>Jumlah top up</th>
					                <th>Action</th>


					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
							    	$nomor=$jumlah['jumlah'];
							    	foreach($model as $data){
						    	?>
						    		<tr class="gradeX">
						    			<td width="30" class="text-right" ><?php echo $nomor;?></td>

										<td><?php echo $data->waktu_top_up;?></td>
										<td><?php echo $data->tanggal_transfer;?></td>
										<td><?php echo $data->jam_transfer;?></td>
										<td><?php echo number_format ($data->jumlah);?>-,</td>
										<td>
											<div class="tooltip-demo">
												<div class="btn-group">
											<?php if($data->status=="diproses"){ ?>		
													<a href="index.php?r=logTopUp/formValidasi&id=<?php echo $data->id_top_up;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Validasi" data-original-title="Tooltip on left" >Validasi</button></a>
													<?php }else{ ?>
													<a href="index.php?r=logTopUp/view&id=<?php echo $data->id_top_up;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Detail" data-original-title="Tooltip on left" >Detail</button></a>
													<?php } ?>
												</div>
											</div>
										</td>

					            	</tr>
					            	<?php
						    	$nomor--;
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>






</div>
</div>