<?php
/* @var $this LogTopUpController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Log Top Ups',
);

$this->menu=array(
	array('label'=>'Create LogTopUp', 'url'=>array('create')),
	array('label'=>'Manage LogTopUp', 'url'=>array('admin')),
);
?>

<h1>Log Top Ups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
