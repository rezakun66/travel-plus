<?php
/* @var $this LogTopUpController */
/* @var $data LogTopUp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_top_up')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->waktu_top_up), array('view', 'id'=>$data->waktu_top_up)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user')); ?>:</b>
	<?php echo CHtml::encode($data->id_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_tranfer')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_tranfer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_tranfer')); ?>:</b>
	<?php echo CHtml::encode($data->jam_tranfer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bukti_tranfer')); ?>:</b>
	<?php echo CHtml::encode($data->bukti_tranfer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>