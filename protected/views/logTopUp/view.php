<?php $level=Yii::app()->session->get('level');
if($level=="admin"){
 ?>

<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Top Up</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Top Up</strong>
					</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo $modelTra->saldo; ?> -,</strong>
				<?php } ?>
				</div>
		</div>
</div>
<?php } ?>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Top Up</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								<table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>Id Top Up</th>
					                <td><?php echo $model->id_top_up;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Waktu Top Up</th>
					                <td><?php echo $model->waktu_top_up;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Pelanggan</th>
					                <td><?php echo $model->customer['nama'];?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Tanggal Transfer</th>
					                <td><?php echo $model->tanggal_transfer;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Jam Transfer</th>
					                <td><?php echo $model->jam_transfer;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Jumlah</th>
					                <td>Rp.<?php echo number_format($model->jumlah);?>-,</td>
					            </tr>
					            <tr class="gradeX">
					                <th>Status</th>
					                <td><?php echo $model->status; ?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Bukti Transfer</th>
					                <td><a href="<?php echo Yii::app()->request->baseUrl; ?>/images/topup/<?php echo $model->bukti_transfer; ?>">
					                	<img alt="image" width="150" height="225" src="<?php echo Yii::app()->request->baseUrl; ?>/images/topup/<?php echo $model->bukti_transfer; ?>" /></a>
					                </td>
					            </tr>
					            


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>