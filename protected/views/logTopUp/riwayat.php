
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Riwayat Top Up</h2>
					            </div>
					            <div class="ibox-content">

					            <a href="index.php?r=LogTopUp/create"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-plus-square"></i> Top Up</button></a>
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Detail</th>


					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
							    	$nomor=$jumlah['jumlah'];
							    	foreach($model as $data){
						    	?>
						    		<tr class="gradeX">
						    			<td width="30" class="text-right" ><?php echo $nomor;?></td>

										<td>Waktu : <?php echo $data->waktu_top_up;?><br>
										Tanggal Transfer : <?php echo $data->tanggal_transfer;?><br>
										Jam Transfer : <?php echo $data->jam_transfer;?><br>
										Jumlah top up : Rp. <?php echo number_format ($data->jumlah);?>-,<br>
										Bukti Transfer : <?php echo $data->bukti_transfer;?><br>
										Status : <?php echo $data->status;?><br>
										<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/topup/<?php echo $data->bukti_transfer; ?>" width="150" height="180">

										<?php if($data->status=="ditolak"){?>
										<br>
										<br>
										
									<div class="tooltip-demo">
                                        <div class="btn-group">
                                            <a href="index.php?r=logTopUp/Update&id=<?php echo $data->id_top_up;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Upload Ulang" data-original-title="Tooltip on left" >Upload Ulang</button></a>
                                        </div>
                                        </div>
										<?php } ?>
                                    </td>

					            	</tr>
					            	<?php
						    	$nomor--;
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>






</div>
</div>