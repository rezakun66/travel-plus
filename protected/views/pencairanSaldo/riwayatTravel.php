<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Pencairan Saldo</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Pencairan Saldo</strong>
					</li>
					<li>
							Riwayat Pencairan
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
				
					<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
				
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Riwayat Pencairan</h2>
					            </div>
					            <div class="ibox-content">

					            <a href="index.php?r=pencairanSaldo/create"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-plus-square"></i> Cairkan Saldo</button></a>
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>Waktu Pencairan</th>
					                <th>Jumlah Pencairan</th>
					                <th>Status</th>
					                <th>action</th>


					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
							    	$nomor=$jumlah['jumlah'];
							    	foreach($model as $data){
						    	?>
						    		<tr class="gradeX">
						    			<td width="30" class="text-right" ><?php echo $nomor;?></td>

										<td><?php echo $data->waktu_pencairan;?></td>
										<td>Rp. <?php echo number_format ($data->jumlah_pencairan-10000);?>-,</td>
										<td><?php echo $data->status;?></td>
										

									<td>	
									<div class="tooltip-demo">
                                        <div class="btn-group">
                                            <a href="index.php?r=pencairanSaldo/view&id=<?php echo $data->id_pencairan ;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Detail" data-original-title="Tooltip on left" >Detail</button></a>
                                        </div>
                                        </div>

                                    </td>

					            	</tr>
					            	<?php
						    	$nomor--;
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>






</div>
</div>