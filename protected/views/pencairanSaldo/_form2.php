<?php
/* @var $this PencairanSaldoController */
/* @var $model PencairanSaldo */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Pencairan</h2>
		            </div>
		            <div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pencairan-saldo-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),	
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_travel'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'id_travel',array('size'=>15,'class'=>'form-control m-b ','readonly'=>true,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'id_travel'); ?>
	</div>
	</div>
	</div>



<?php $model2=Travel::model()->findByPk($model->id_travel); ?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label">Travel</label>
		 	<div class="col-sm-6">
		 		<input type="text" class="form-control m-b" readonly="true" value="<?php echo $model2->nama_travel; ?>" >
	</div>
	</div>
	</div>
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label">Kode Bank</label>
		 	<div class="col-sm-6">
		 		<input type="text" class="form-control m-b" readonly="true" value="<?php echo $model2->kode_bank; ?>" >
	</div>
	</div>
	</div>
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label">nomor rekening</label>
		 	<div class="col-sm-6">
		 		<input type="text" class="form-control m-b" readonly="true" value="<?php echo $model2->no_rekening; ?>" >
	</div>
	</div>
	</div>
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label">Jumlah Pencairan</label>
		 	<div class="col-sm-6">
		 		<input type="text" class="form-control m-b" readonly="true" value="Rp. <?php echo number_format($model->jumlah_pencairan-10000); ?>-," >
	</div>
	</div>
	</div>



	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label">Tanggal Transfer </label>
		 	<div class="col-sm-6">
		<input type="date" name="tgl_trf" class="form-control m-b" >
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label">Jam Transfer</label>
		 	<div class="col-sm-6">
		<input type="time" name="jam_trf" class="form-control m-b" >
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'bukti_transfer'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->fileField($model,'bukti_transfer'); ?>
		<?php echo $form->error($model,'bukti_transfer'); ?>
	</div>
	</div>
	</div>


		<?php echo $form->hiddenField($model,'status',array('size'=>10,'maxlength'=>10,'value'=>'ditransfer')); ?>
		<?php echo $form->error($model,'status'); ?>
	
		<?php echo $form->hiddenField($modelTrav,'id_travel'); ?>
		<?php echo $form->error($modelTrav,'id_travel'); ?>

<br>
	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Validasi', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->