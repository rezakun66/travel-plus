<?php
/* @var $this PencairanSaldoController */
/* @var $data PencairanSaldo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pencairan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pencairan), array('view', 'id'=>$data->id_pencairan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_pencairan')); ?>:</b>
	<?php echo CHtml::encode($data->waktu_pencairan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_travel')); ?>:</b>
	<?php echo CHtml::encode($data->id_travel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_pencairan')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_pencairan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_tranfer')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_tranfer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_tranfer')); ?>:</b>
	<?php echo CHtml::encode($data->jam_tranfer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bukti_tranfer')); ?>:</b>
	<?php echo CHtml::encode($data->bukti_tranfer); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_selesai')); ?>:</b>
	<?php echo CHtml::encode($data->waktu_selesai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>