<?php
/* @var $this PencairanSaldoController */
/* @var $model PencairanSaldo */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Pencairan</h2>
		            </div>
		            <div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pencairan-saldo-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_travel'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'id_travel',array('size'=>15,'class'=>'form-control m-b ','readonly'=>true,'maxlength'=>15,'value'=>Yii::app()->session->get('id'))); ?>
		<?php echo $form->error($model,'id_travel'); ?>
	</div>
	</div>
	</div>
<?php $model2=Travel::model()->findByPk(Yii::app()->session->get('id')); ?>
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label">nomor rekening</label>
		 	<div class="col-sm-6">
		 		<input type="text" class="form-control m-b" readonly="true" value="<?php echo $model2->no_rekening; ?>" >
	</div>
	</div>
	</div>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'jumlah_pencairan'); ?> </label>
		 	<div class="col-sm-6">

		 	<?php echo $form->dropDownList($model,'jumlah_pencairan',array(
				'110000'=> '110.000 saldo = Rp.100.000-, ',
				'210000'=> '210.000 saldo = Rp.200.000-, ',
				'310000'=> '310.000 saldo = Rp.300.000-, ',
				'410000'=> '410.000 saldo = Rp.400.000-, ',
				'510000'=> '510.000 saldo = Rp.500.000-,',
				'1010000'=> '1.010.000 saldo = Rp.1.000.000-,',
				
			),
				array('empty'=>'','class'=>'form-control m-b ','required'=>'" "')); ?>		
		<?php echo $form->error($model,'jumlah_pencairan'); ?>
	</div>
	</div>
	</div>


		<?php echo $form->hiddenField($model,'status',array('size'=>10,'maxlength'=>10,'value'=>'diproses')); ?>
		<?php echo $form->error($model,'status'); ?>
	

	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->