<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>Pencairan Saldo</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Pencairan Saldo</strong>
					</li>
					<li>
							Lihat Detail
					</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">
				<?php 
				$level=Yii::app()->session->get('level');
				$id=Yii::app()->session->get('id');
				
				
				if($level=="travel"){
					$modelTra=Travel::model()->findByPk($id);
					?>
					
				<strong>Saldo : Rp.<?php echo number_format($modelTra->saldo); ?> -,</strong>
				<?php } ?>
				</div>
				
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data Pencairan</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
							<!-- 	<p align="center" ><img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/pelanggan/<?php //echo $model->foto;?>" width="200" height="200" > </p> -->
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>waktu pencairan</th>
					                <td><?php echo $model->waktu_pencairan;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>id travel</th>
					                <td><?php echo $model->id_travel;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>nomor rekening</th>
					                <td><?php echo $model->travel['no_rekening'];?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>jumlah pencairan</th>
					                <td>Rp.<?php $tot=$model->jumlah_pencairan-10000;
					                echo number_format($tot);
					                ?> -,</td>
					            </tr>
					            <tr class="gradeX">
					                <th>status</th>
					                <td><?php echo $model->status;?></td>
					            </tr>
					            <?php if($model->status=="ditransfer"){ ?>
					            <tr class="gradeX">
					                <th>tanggal_transfer</th>
					                <td><?php echo $model->tanggal_transfer;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>jam_transfer</th>
					                <td><?php echo $model->jam_transfer;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>waktu selesai</th>
					                <td><?php echo $model->waktu_selesai;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Bukti Transfer</th>
					                <td><a href="<?php echo Yii::app()->request->baseUrl; ?>/images/pencairan/<?php echo $model->bukti_transfer; ?>">
					                	<img alt="image" width="150" height="225" src="<?php echo Yii::app()->request->baseUrl; ?>/images/pencairan/<?php echo $model->bukti_transfer; ?>" /></a>
					                </td>
					            </tr>
					            <?php } ?>
						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>