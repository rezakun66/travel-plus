<?php
/* @var $this PencairanSaldoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pencairan Saldos',
);

$this->menu=array(
	array('label'=>'Create PencairanSaldo', 'url'=>array('create')),
	array('label'=>'Manage PencairanSaldo', 'url'=>array('admin')),
);
?>

<h1>Pencairan Saldos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
