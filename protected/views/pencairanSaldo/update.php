<?php
/* @var $this PencairanSaldoController */
/* @var $model PencairanSaldo */

$this->breadcrumbs=array(
	'Pencairan Saldos'=>array('index'),
	$model->id_pencairan=>array('view','id'=>$model->id_pencairan),
	'Update',
);

$this->menu=array(
	array('label'=>'List PencairanSaldo', 'url'=>array('index')),
	array('label'=>'Create PencairanSaldo', 'url'=>array('create')),
	array('label'=>'View PencairanSaldo', 'url'=>array('view', 'id'=>$model->id_pencairan)),
	array('label'=>'Manage PencairanSaldo', 'url'=>array('admin')),
);
?>

<h1>Update PencairanSaldo <?php echo $model->id_pencairan; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>