<?php
/* @var $this PencairanSaldoController */
/* @var $model PencairanSaldo */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_pencairan'); ?>
		<?php echo $form->textField($model,'id_pencairan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'waktu_pencairan'); ?>
		<?php echo $form->textField($model,'waktu_pencairan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_travel'); ?>
		<?php echo $form->textField($model,'id_travel',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jumlah_pencairan'); ?>
		<?php echo $form->textField($model,'jumlah_pencairan',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tanggal_tranfer'); ?>
		<?php echo $form->textField($model,'tanggal_tranfer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jam_tranfer'); ?>
		<?php echo $form->textField($model,'jam_tranfer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bukti_tranfer'); ?>
		<?php echo $form->textField($model,'bukti_tranfer',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'waktu_selesai'); ?>
		<?php echo $form->textField($model,'waktu_selesai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->