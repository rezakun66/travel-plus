
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Riwayat Pemesanan</h2>
					            </div>
					            <div class="ibox-content">

					            <div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>

					                <th>No</th>
					                <th>Info</th>

					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
							    	$no=$jumlah['jumlah'];
							    	foreach($model as $data){
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $no;?></td>
						    			<td>
										Travel : <?php echo $data->schedule->car->trav['nama_travel'];?><br>
                                        
                                        Tanggal berangkat :<?php echo $data->schedule['tanggal'];?><br>
						    			Jam berangkat :<?php echo $data->schedule['jam'];?><br>
										
                                        Pembayaran  :   Rp.<?php 
                                        if($data->status=="lunas"){
                                        echo number_format( $data->pembayaran); 
                                    }else{
                                    	$bagi2=$data->pembayaran/2;
                                    	echo number_format( $bagi2);
                                    }
                                        ?>-, <br>
                                        waktu pesan :<?php echo $data->waktu_pesan;?><br>
                                         <?php if($data->status=="batal"){ ?>
                                        waktu batal :<?php echo $data->waktu_batal;?><br> 
                                        <?php } ?>
                                        Status :<?php if($data->status=="batal"){ ?> 
                                            <b style="color:red;"><?php echo $data->status;?></b><br>
                                        <?php }else{ ?>
											<b style="color:green;"><?php echo $data->status;?></b><br>	
										<?php } ?>
										<br>
										<?php
										$yeterday= date_create('-1 day')->format('d-m-Y H:i:s');
										$today= new DateTime(date('d-m-Y'));
										$expired= new DateTime($data->schedule['tanggal']);

										 if($expired>$today & $data->status=="lunas"){
										 	?>
										 	<a href="index.php?r=pemesanan/batal2&id=<?php echo $data->id_pesan;?>"> <button class="btn-danger btn btn-xs" data-toggle="tooltip" data-placement="right" title="Batalkan pesanan" data-original-title="Tooltip on right" onclick="return confirm('Jika dibatalkan saldo hanya dikembalikan setengah total pembayaran?')" >Batalkan</button></a>

										 	<?php
										 }

										 ?>
												  
<a href="index.php?r=pemesanan/detailPesan&id=<?php echo $data->id_pesan;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="right" title="Detail pesanan" data-original-title="Tooltip on right" >Detail</button></a>												  
												  </td>
						    		

					            	</tr>
					            	<?php
						    	$no--;
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>






</div>
</div>