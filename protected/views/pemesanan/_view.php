<?php
/* @var $this PemesananController */
/* @var $data Pemesanan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pesan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pesan), array('view', 'id'=>$data->id_pesan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jadwal')); ?>:</b>
	<?php echo CHtml::encode($data->id_jadwal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pelanggan')); ?>:</b>
	<?php echo CHtml::encode($data->id_pelanggan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lat_jemput')); ?>:</b>
	<?php echo CHtml::encode($data->lat_jemput); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('long_jemput')); ?>:</b>
	<?php echo CHtml::encode($data->long_jemput); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>