<?php
/* @var $this PemesananController */
/* @var $model Pemesanan */

$this->breadcrumbs=array(
	'Pemesanans'=>array('index'),
	$model->id_pesan,
);

$this->menu=array(
	array('label'=>'List Pemesanan', 'url'=>array('index')),
	array('label'=>'Create Pemesanan', 'url'=>array('create')),
	array('label'=>'Update Pemesanan', 'url'=>array('update', 'id'=>$model->id_pesan)),
	array('label'=>'Delete Pemesanan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pesan),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pemesanan', 'url'=>array('admin')),
);
?>

<h1>View Pemesanan #<?php echo $model->id_pesan; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pesan',
		'id_jadwal',
		'id_pelanggan',
		'lat_jemput',
		'long_jemput',
		'status',
	),
)); ?>
