
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Pemesanan</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <tr class="gradeX">
					                <th>Travel</th>
					                <td><?php echo $model->schedule->car->trav['nama_travel'];?></td>
					            </tr>					            
					            <tr class="gradeX">
					                <th>Tanggal</th>
					                <td><?php echo $model->schedule['tanggal'];?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Jam</th>
					                <td><?php echo $model->schedule['jam'];?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Alamat Jemput</th>
					                <td><?php echo $model->alamat_jemput;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Pembayaran</th>
					                <td>Rp. <?php echo number_format ($model->pembayaran);?>-,</td>
					            </tr>
					            <tr class="gradeX">
					                <th>Status</th>
					                <td><?php echo $model->status;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>Posisi kursi</th>
					                <td><?php  $model2=Kursi::model()->findAll(array('condition'=>'id_pesan="'.$model->id_pesan.'"'));
									foreach($model2 as $dat){
										echo $dat->kursi."</br>";
									}
									?></td>
					            </tr>

					            


						    	</table>
								
								
								<a href="index.php?r=pemesanan/riwayat"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-long-arrow-right"></i> NEXT</button></a>
								
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>