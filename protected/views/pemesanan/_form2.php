<?php
/* @var $this PemesananController */
/* @var $model Pemesanan */
/* @var $form CActiveForm */
?>

<div class="form">


<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Pemesanan</h2>
		            </div>
		            <div class="ibox-content">	

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pemesanan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>



		<?php echo $form->hiddenField($model,'id_pesan',array('class'=>'form-control m-b','value'=>1)); ?>
		<?php echo $form->error($model,'id_pesan'); ?>


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_jadwal'); ?> </label>
		 	<div class="col-sm-6">
		<?php 
		$a=Yii::app()->request->getParam('id');
		echo $form->textField($model,'id_jadwal',array('class'=>'form-control m-b','value'=>$a,'readonly'=>true)); ?>
		<?php echo $form->error($model,'id_jadwal'); ?>
	</div>
	</div>
	</div>
<?php 
$krsicc=false;
$krsi1=false;
$krsi2=false;
$krsi3=false;
$krsi4=false;

$modelChair=Kursi::model()->findAll(array('condition'=>'id_jadwal = "'.$a.'" and status != "batal"'));
foreach($modelChair as $Chair){
	if($Chair->kursi=="cc"){
		$krsicc=true;
	}else if($Chair->kursi=="1"){
		$krsi1=true;
	}else if($Chair->kursi=="2"){
		$krsi2=true;
	}else if($Chair->kursi=="3"){
		$krsi3=true;
	}else if($Chair->kursi=="4"){
		$krsi4=true;
	}
}

/*$count=2;
			for( $i=1; $i <= $count; $i++ ){
				echo $i;
			}
			$idq=Yii::app()->request->getParam('id');
			         */
?>

	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo 'Pilih kursi*' ?> </label>
		 	<div class="col-sm-2">
			<table class="table table-striped table-bordered table-hover dataTables-example dataTable" >
				<tr>
				<?php if($krsicc==false){ ?>
				<td bgcolor="#00CED1" ><div class="i-checks"><label>CC <input type="checkbox" name="kursiCC" value="cc"> <i></i></label></div></td>
				<?php }else{?>
				<td bgcolor="#FF3800" ><div class="i-checks"><label>CC <input type="checkbox" name="kursiCC" disabled="" checked="" value="x"> <i></i></label></div></td>
				<?php } ?>
				<td bgcolor="#F0EAD6"><label>Driver</label></td>
				</tr>
				<tr>
				<?php if($krsi1==false){ ?>
				<td bgcolor="#00CED1" ><div class="i-checks"><label>1 <input type="checkbox" name="kursi1" value="1"> <i></i></label></div></td>
				<?php }else{ ?>
				<td bgcolor="#FF3800" ><div class="i-checks"><label>1 <input type="checkbox" name="kursi1" disabled="" checked="" value="x"> <i></i></label></div></td>
				<?php } ?>
				
				<?php if($krsi2==false){ ?>
				<td bgcolor="#00CED1" ><div class="i-checks"><label>2 <input type="checkbox" name="kursi2" value="2"> <i></i></label></div></td>
				<?php }else{ ?>
				<td bgcolor="#FF3800" ><div class="i-checks"><label>2 <input type="checkbox" name="kursi2" disabled="" checked="" value="x"> <i></i></label></div></td>
				<?php } ?>
				
				
				</tr>
				<tr>

				<?php if($krsi3==false){ ?>
				<td bgcolor="#00CED1" ><div class="i-checks"><label>3 <input type="checkbox" name="kursi3" value="3"> <i></i></label></div></td>
				<?php }else{ ?>
				<td bgcolor="#FF3800" ><div class="i-checks"><label>3 <input type="checkbox" name="kursi3" disabled="" checked="" value="x"> <i></i></label></div></td>
				<?php } ?>
				
				<?php if($krsi4==false){ ?>
				<td bgcolor="#00CED1" ><div class="i-checks"><label>4 <input type="checkbox" name="kursi4" value="ayam"> <i></i></label></div></td>
				<?php }else{ ?>
				<td bgcolor="#FF3800" ><div class="i-checks"><label>4 <input type="checkbox" name="kursi4" disabled="" checked="" value="x"> <i></i></label></div></td>
				<?php } ?>
				
				
				</tr>

			</table>
	</div>
	</div>
	</div>		
	
		<?php echo $form->hiddenField($model,'id_pelanggan',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b','value'=>Yii::app()->user->getId())); ?>
		<?php echo $form->error($model,'id_pelanggan'); ?>

		
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'alamat_jemput'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textArea($model,'alamat_jemput',array('class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'alamat_jemput'); ?>
	</div>
	</div>
	</div>


	
	
		<?php echo $form->hiddenField($model,'pembayaran',array('value'=>1000,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'pembayaran'); ?>

	


		<?php echo $form->hiddenField($model,'status',array('size'=>10,'maxlength'=>10,'class'=>'form-control m-b','value'=>'diproses')); ?>
		<?php echo $form->error($model,'status'); ?>

		
		<?php echo $form->hiddenField($model2,'id_kursi',array('class'=>'form-control m-b','value'=>1)); ?>
		<?php echo $form->error($model2,'id_kursi'); ?>
	
	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Selanjutnya' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->