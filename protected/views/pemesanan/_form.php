<?php
/* @var $this PemesananController */
/* @var $model Pemesanan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pemesanan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_pesan'); ?>
		<?php echo $form->textField($model,'id_pesan'); ?>
		<?php echo $form->error($model,'id_pesan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_jadwal'); ?>
		<?php echo $form->textField($model,'id_jadwal'); ?>
		<?php echo $form->error($model,'id_jadwal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_pelanggan'); ?>
		<?php echo $form->textField($model,'id_pelanggan',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'id_pelanggan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lat_jemput'); ?>
		<?php echo $form->textField($model,'lat_jemput'); ?>
		<?php echo $form->error($model,'lat_jemput'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'long_jemput'); ?>
		<?php echo $form->textField($model,'long_jemput'); ?>
		<?php echo $form->error($model,'long_jemput'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->