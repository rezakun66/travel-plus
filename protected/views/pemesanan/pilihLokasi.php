<?php
/* @var $this PemesananController */
/* @var $model Pemesanan */
/* @var $form CActiveForm */
?>

<div class="form">


<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Pemesanan</h2>
		            </div>
		            <div class="ibox-content">	


<?php 
$id=Yii::app()->session->get('id'); 
if($model->id_pelanggan==$id){ ?>
		
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> Pilih Lokasi </label>
	 	
       
	   <div  class="col-sm-6" >
        <div style="padding:10px" >
            <div id="map" class="col-sm-6" ></div>
        </div>
        </div>
        
        </div>
        </div>			

        
        <script type="text/javascript">
        var map;
        var latit = 0.4000001; // YOUR LATITUDE VALUE
        var longit = 101.300001; // YOUR LONGITUDE VALUE
        function initMap() {                            
            var latitude = 0.458335; // YOUR LATITUDE VALUE
            var longitude = 101.394172; // YOUR LONGITUDE VALUE                           

            
            var myLatLng = {lat: latitude, lng: longitude};
            
            map = new google.maps.Map(document.getElementById('map'), {
              center: myLatLng,
              zoom: 12,
              disableDoubleClickZoom: true, // disable the default map zoom on double click

            });
            
            // Update lat/long value of div when anywhere in the map is clicked    
            google.maps.event.addListener(map,'click',function(event) {                
                document.getElementById('latclicked').innerHTML = event.latLng.lat();
                document.getElementById('longclicked').innerHTML =  event.latLng.lng();
            });
            
            // Update lat/long value of div when you move the mouse over the map
            google.maps.event.addListener(map,'mousemove',function(event) {
                document.getElementById('latmoved').innerHTML = event.latLng.lat();
                document.getElementById('longmoved').innerHTML = event.latLng.lng();
            });
                    
            var marker = new google.maps.Marker({
              //position: myLatLng,
              map: map,
              //title: 'Hello World'
              
              // setting latitude & longitude as title of the marker
              // title is shown when you hover over the marker
              title: latitude + ', ' + longitude 
            });    
            
            // Update lat/long value of div when the marker is clicked
            marker.addListener('click', function(event) {              
              document.getElementById('latclicked').innerHTML = event.latLng.lat();
              document.getElementById('longclicked').innerHTML =  event.latLng.lng();
            });
            
            // Create new marker on double click event on the map
            google.maps.event.addListener(map,'dblclick',function(event) {
                var marker = new google.maps.Marker({
                  position: event.latLng, 
                  map: map, 
                  title: event.latLng.lat()+', '+event.latLng.lng()
                });
                latit= event.latLng.lat();
				longit=  event.latLng.lng();
                // Update lat/long value of div when the marker is clicked
                marker.addListener('click', function() {
                  document.getElementById('latclicked').innerHTML = event.latLng.lat();
                  document.getElementById('longclicked').innerHTML =  event.latLng.lng();
                });            
            });
            
            // Create new marker on single click event on the map
            /*google.maps.event.addListener(map,'click',function(event) {
                var marker = new google.maps.Marker({
                  position: event.latLng, 
                  map: map, 
                  title: event.latLng.lat()+', '+event.latLng.lng()
                });                
            });*/
        }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3Z9HSojIM2qZ15U5BHgJJJKmSUYstzEY&callback=initMap"
        async defer></script>
	


	
		<font color="white" id='latclicked'></font>
        <font color="white" id="longclicked"></font>
        
		
        <font color="white" id="latmoved"></font>
        <font color="white" id="longmoved"></font>
	
	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> </label>
		 	<div class="col-sm-6">
			<h3>Total Pembayaran Rp.<?php echo number_format($model->pembayaran); ?>-,</h3>
	</div>
	</div>
	</div>
	
	
	
	
	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
			
			<button class="btn btn-primary" onclick=" javascript:window.location.href = 'index.php?r=pemesanan/simpan&id=<?php echo $model->id_pesan; ?>&lat=' + latit +'&lon='+ longit;" >Pesan
			</button>			
			<button class="btn btn-danger" onclick="javascript:window.location.href = 'index.php?r=pemesanan/batal&id=<?php echo $model->id_pesan; ?>'">Batalkan
			</button>
		
	</div>
	</div>
	</div>

<?php } ?>

</div><!-- form -->

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->