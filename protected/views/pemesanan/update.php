<?php
/* @var $this PemesananController */
/* @var $model Pemesanan */

$this->breadcrumbs=array(
	'Pemesanans'=>array('index'),
	$model->id_pesan=>array('view','id'=>$model->id_pesan),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pemesanan', 'url'=>array('index')),
	array('label'=>'Create Pemesanan', 'url'=>array('create')),
	array('label'=>'View Pemesanan', 'url'=>array('view', 'id'=>$model->id_pesan)),
	array('label'=>'Manage Pemesanan', 'url'=>array('admin')),
);
?>

<h1>Update Pemesanan <?php echo $model->id_pesan; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>