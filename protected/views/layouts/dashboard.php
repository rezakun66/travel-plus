<?php  
$level=Yii::app()->session->get('level');
$id=Yii::app()->session->get('id');
/*$model1=HakAkses::model()->findByPk($id);
//$model->nama_user; */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>travel pekanbaru</title>

	 <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('6ffb85509d8fbbaf2f63', {
      cluster: 'ap1',
      forceTLS: true
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function(data) {
      alert(JSON.stringify(data));
    });
  </script>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/animate.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/style.css" rel="stylesheet">

    <!-- date picker-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

	 <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">


    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	        <style>
        @media (min-width: 350px) {
            .xxx{
                border:1px solid white;
                width:100%;
                height:100%;
                overflow-y:auto;
                overflow-x:auto;
            }
        }
		</style>
</head>

    <!-- <body class="pace-done mini-navbar"> -->
<body>
    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu" style="display:block;">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                      <?php if($level=='admin'){ ?>
                            <img alt="image" width="105" height="105" class="img-circle" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/img/a4.jpg" />

                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">admin </strong>
                             </span> <span class="text-muted text-xs block"> profile <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                          <?php //if($level=='admin'){ ?>
                            <li><a href="index.php?r=mahasiswa/profile">Profile</a></li>
                            <?php //} ?>
                            <li class="divider"></li>
                            <li><a href="index.php?r=site/logout">Logout</a></li>

                        </ul>
                        <?php } else if($level=='travel'){
                          $modelss=Travel::model()->findByPk($id);
                          ?>

                            <img alt="image" width="105" height="105" class="img-circle" src="<?php echo Yii::app()->request->baseUrl; ?>/images/travel/<?php echo $modelss->logo; ?>" />

                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $modelss->nama_travel; ?> </strong>
                             </span> <span class="text-muted text-xs block"> profile <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                         
                            <li><a href="index.php?r=mahasiswa/profile">Profile</a></li>
                          
                            <li class="divider"></li>
                            <li><a href="index.php?r=site/logout">Logout</a></li>

                        </ul>
                        <?php } else if($level=='supir'){
                          $modelss=Supir::model()->findByPk($id);
                          ?>


                            <img alt="image" width="105" height="105" class="img-circle" src="<?php echo Yii::app()->request->baseUrl; ?>/images/supir/<?php echo $modelss->foto; ?>" />

                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $modelss->nama; ?></strong>
                             </span> <span class="text-muted text-xs block"> profile <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                         
                            <li><a href="index.php?r=supir/profile">Profile</a></li>
                          
                            <li class="divider"></li>
                            <li><a href="index.php?r=site/logout">Logout</a></li>

                        </ul>
                        <?php } ?>

                    </div>
                    <div class="logo-element">
                        TP+
                    </div>
                </li>
				        
                <?php
      					if($level=='admin'){

      					?>
                <li>
                    <a href="index.php?r=dashboard/dashboardAdmin"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
                </li>

                <li>
                    <a href="index.html"><i class="fa fa-database"></i> <span class="nav-label">Data Master</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php?r=kota/admin">Kota</a></li>
                        <li><a href="index.php?r=pelanggan/admin">Pelanggan</a></li>
                        <li><a href="index.php?r=travel/admin">Travel</a></li>
                        <li><a href="index.php?r=user/admin">User</a></li>
                    </ul>
                </li>
                <li>
                    <a href="index.html"><i class="fa fa-btc"></i> <span class="nav-label">Top Up Saldo</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php?r=logTopUp/validasi">Validasi</a></li>
                        <li><a href="index.php?r=logTopUp/terima">Daftar Terima</a></li>
                        <li><a href="index.php?r=logTopUp/tolak">Daftar Tolak</a></li>
                        
                    </ul>
                </li>

        <li>
            <a href="index.html"><i class="fa fa-dollar"></i> <span class="nav-label">Pencairan Saldo</span> <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li><a href="index.php?r=pencairanSaldo/validasi">Validasi</a></li>
                <li><a href="index.php?r=pencairanSaldo/riwayatAdmin">Riwayat</a></li>
            </ul>
        </li>				



				<li>
					<a href="index.php?r=laporan/laporanAdmin&tgl=<?php echo date('Y-m-d'); ?>&tgl2=<?php echo date('Y-m-d'); ?>"><i class="fa fa-file-o"></i> <span class="nav-label">Laporan Keuangan</span> <span ></span></a>

                </li>
				<?php }else if($level=='travel'){ ?>

                <li>
                    <a href="index.php?r=dashboard/dashboardTravel"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
                </li>


				<li>
					<a href="index.php?r=supir/admin"><i class="fa fa-users"></i> <span class="nav-label">Supir</span> <span ></span></a>

                </li>
				<li>
					<a href="index.php?r=mobil/admin"><i class="fa fa-car"></i> <span class="nav-label">Mobil</span> <span ></span></a>

                </li>
				<li>
					<a href="index.php?r=hargaTujuan/admin"><i class="fa fa-money"></i> <span class="nav-label">Harga Tujuan</span> <span ></span></a>

        </li>
				<li>
					<a href="index.php?r=jadwal/admin"><i class="fa fa-calendar"></i> <span class="nav-label">Penjadwalan</span> <span ></span></a>

        </li>
				

        <li>
            <a href="index.html"><i class="fa fa-dollar"></i> <span class="nav-label">Pencairan Saldo</span> <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li><a href="index.php?r=pencairanSaldo/riwayatTravel">Riwayat</a></li>
                <li><a href="index.php?r=pencairanSaldo/create">Cairkan Saldo</a></li>
            </ul>
        </li>

				<li>
					<a href="index.php?r=laporan/laporanTravel&tgl=<?php echo date('Y-m-d'); ?>&tgl2=<?php echo date('Y-m-d'); ?>"><i class="fa fa-file"></i> <span class="nav-label">Laporan</span> <span ></span></a>

        </li>

				<?php }else if($level=='supir'){  ?>

                <li>
                    <a href="index.php?r=dashboard/dashboardSupir"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
                </li>
                <li>
                    <a href="index.php?r=supir/profile"><i class="fa fa-user"></i> <span class="nav-label">Profile</span></a>
                </li>

        <li>
          <a href="index.php?r=jadwal/jadwalSupir"><i class="fa fa-calendar"></i> <span class="nav-label">Jadwal</span> <span ></span></a>

                </li>

                <?php } ?>
            </ul>
            

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			<!--Token : Rp 150000-,
             <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form> search -->
        </div>
            <ul class="nav navbar-top-links navbar-right">
                
<?php if($level=="admin"){ 
$notif1=LogTopUp::model()->notif();
$notif2=pencairanSaldo::model()->notif();
$totnotif=$notif1+$notif2;
?>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i> <?php if($totnotif>0){ ?> <span class="label label-primary"><?php echo $totnotif; ?></span><?php } ?>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                      <?php if($notif1>0){ ?>
                        <li>
                            <a href="index.php?r=logTopUp/validasi">
                                <div>
                                    <i class="fa fa-btc fa-fw"></i> <?php echo $notif1;  ?> permintaan validasi top up
                                   
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <?php }

                        if($notif2>0){
                         ?>
                        <li>
                            <a href="index.php?r=pencairanSaldo/validasi">
                                <div>
                                    <i class="fa fa-dollar fa-fw"></i> <?php echo $notif2;  ?> permintaan pencairan saldo
                                </div>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
<?php } ?>

                <li>
                    <a href="index.php?r=site/logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>





<?php echo $content; ?>




            <div class="footer">

                <div>
                    <strong>Copyright</strong> Travel pekanbaru &copy; 2018 
                </div>
            </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/jquery-2.1.1.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/inspinia.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/pace/pace.min.js"></script>

	
	
        <!-- data table -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/dataTables/datatables.min.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/chosen/chosen.jquery.js"></script>
 <!-- Select2 -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/select2/select2.full.min.js"></script>

    <!-- Data picker -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>

        <!-- Clock picker -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/clockpicker/clockpicker.js"></script>

		
		
		
     <script>

     $(document).ready(function(){
                 $('.dataTables-example').DataTable({
                     dom: '<"html5buttons"B>lTfgitp',
                     buttons: [
                         
                     ]



                 });

                 $('.clockpicker').clockpicker(
                   {
                     forceParse: false,
                     format: "HH/ii/SS"
                   }
                 );

                 $('#data_5 .input-daterange').datepicker({
                     keyboardNavigation: false,
                     forceParse: false,
                     autoclose: true,
                     format: "yyyy/mm/dd"
                 });

                 var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
                }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }

			$(".select2_demo_1").select2();
            $(".select2_demo_2").select2();
            $(".select2_demo_3").select2({
                placeholder: "Select a state",
                allowClear: true
            });
                 /* Init DataTables */
                 var oTable = $('#editable').DataTable();

                 /* Apply the jEditable handlers to the table */
                 oTable.$('td').editable( '../example_ajax.php', {
                     "callback": function( sValue, y ) {
                         var aPos = oTable.fnGetPosition( this );
                         oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                     },
                     "submitdata": function ( value, settings ) {
                         return {
                             "row_id": this.parentNode.getAttribute('id'),
                             "column": oTable.fnGetPosition( this )[2]
                         };
                     },

                     "width": "90%",
                     "height": "100%"
                 } );
});


     </script>
</body>

</html>
