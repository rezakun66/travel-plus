<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Travel</title>

	<script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/animate.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/style.css" rel="stylesheet">

    <!-- date picker-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

	 <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
	        <style>
        @media (min-width: 350px) {
            .cuari{

                width:300px;
            }
			.xxx{
                border:1px solid white;
                width:100%;
                height:100%;
                overflow-y:auto;
                overflow-x:auto;
            }
        }
		.ayammm{
			border: 1px solid black
		}
		#map { 
					
			width: 100%;
			height: 400px;
			margin: 0;
			padding: 10px;		
			position: relative;
          }
		
		</style>	
		        
</head>

<body class="top-navigation">
<?php 
$id=Yii::app()->session->get('id'); 
$modelCus=Pelanggan::model()->findByPk($id);
?>
    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <a href="index.php?r=dashboard/dashboard" class="navbar-brand">TRAVEL PEKANBARU</a>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <!-- <li class="active">
                        <a aria-expanded="false" role="button" href="index.php?r=dashboard/dashboard"> </a>
                    </li>
					 -->
                    <li class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ticket  "></i>Pemesanan<span class="caret"></span></a>
                        <ul role="menu" class="dropdown-menu">
                            
                            <li><a href="index.php?r=dashboard/dashboard">Daftar Tujuan</a></li>
                            <li><a href="index.php?r=pemesanan/riwayat">Riwayat</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-btc  "></i>Top Up Saldo<span class="caret"></span></a>
                        <ul role="menu" class="dropdown-menu">
                            
                            <li><a href="index.php?r=logTopUp/create">Tambah Saldo</a></li>
                            <li><a href="index.php?r=logTopUp/daftarHarga">Daftar Harga</a></li>
							<li><a href="index.php?r=logTopUp/riwayat">Riwayat</a></li>
                        </ul>
                    </li>



                </ul>
				<ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="index.php?r=site/logout">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>
				
            </div>
        </nav>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container">
            <div class="row">
        

            <div class="row m-b-lg m-t-lg">
							<div class="col-md-6">

								<div class="profile-image">
									<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/pelanggan/<?php echo $modelCus->foto; ?>" class="img-circle circle-border m-b-md" alt="profile">
								</div>
								<div class="profile-info">
									<div class="">
										<div>
											<h2 class="no-margins">
												<?php echo $modelCus->nama; ?>
											</h2>
											<br>
											<a href="index.php?r=pelanggan/editProfile">
											<button class="btn btn-warning " type="button"><i class="fa fa-pencil"></i>&nbsp;edit profile</button>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<table class="table small m-b-xs">
									<tbody>
									<tr><td>
										<strong>nomor telp</td><td> <?php echo $modelCus->no_telp; ?></strong>
										</td>
									</tr>
									<tr>
										<td>
											<strong>jenis kelamin</td><td> <?php if($modelCus->jenis_kelamin=="L"){
												echo "Laki-laki";
											}else{ echo "Perempuan"; } ?></strong> 
										</td>
										
									</tr>
									<tr>
										<td>
											<strong>alamat</td><td> <?php echo $modelCus->alamat; ?></strong> 
										</td>
										
									</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-3">
								<small>Jumlah Saldo</small>
								<h2 class="no-margins">Rp. <?php echo number_format ($modelCus->saldo); ?> -,</h2>
								<div id="sparkline1"></div>
							</div>


            </div>
<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">


					

<?php echo $content; ?>




            </div>


                

            </div>

        </div>
				<div class="footer">
					
					<div>
						<strong>Copyright</strong> travel pekanbaru &copy; 2018
					</div>
				</div>

			</div>
		</div>
    </div>
</div>



<!-- Mainly scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/jquery-2.1.1.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/bootstrap.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/inspinia.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

	

    <!-- data table -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/dataTables/datatables.min.js"></script>


    <!-- iCheck -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/iCheck/icheck.min.js"></script>
    
    <script>
     $(document).ready(function(){
		 
				 $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
				
                 $('.dataTables-example').DataTable({
                     dom: '<"html5buttons"B>lTfgitp',
                     buttons: [
                         
                     ]



                 });

                 $('.clockpicker').clockpicker(
                   {
                     forceParse: false,
                     format: "HH/ii/SS"
                   }
                 );

                 $('#data_5 .input-daterange').datepicker({
                     keyboardNavigation: false,
                     forceParse: false,
                     autoclose: true,
                     format: "yyyy/mm/dd"
                 });

                 var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"50%"}
                }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }

			$(".select2_demo_1").select2();
            $(".select2_demo_2").select2();
            $(".select2_demo_3").select2({
                placeholder: "Select a state",
                allowClear: true
            });
                 /* Init DataTables */
                 var oTable = $('#editable').DataTable();

                 /* Apply the jEditable handlers to the table */
                 oTable.$('td').editable( '../example_ajax.php', {
                     "callback": function( sValue, y ) {
                         var aPos = oTable.fnGetPosition( this );
                         oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                     },
                     "submitdata": function ( value, settings ) {
                         return {
                             "row_id": this.parentNode.getAttribute('id'),
                             "column": oTable.fnGetPosition( this )[2]
                         };
                     },

                     "width": "90%",
                     "height": "100%"
                 } );
});

    </script>

</body>

</html>
