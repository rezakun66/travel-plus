<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>TRAVEL PEKANBARU</title>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/animate.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/style.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">



    <!-- date picker-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

</head>

<body class="gray-bg">
  <a href="index.php"><button class="btn btn-primary dim btn-large-dim animated bounceIn" style="margin: 20px; position:fixed; "><i class="fa fa-home"></i></button></a>
    <div class="middle-box text-center    animated bounceIn">
        <div>
            <div>

                <h2 class="logo-name ">TP+</h2>

            </div>
            <p></p>
<?php echo $content; ?>
            <p class="m-t"> <small>TRAVEL PEKANBARU</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/jquery-2.1.1.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/iCheck/icheck.min.js"></script>
    <!-- Input Mask-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <!-- Data picker -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Sweet alert -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/inspinia/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });
            $('#data_2 .input-group.date').datepicker({
                startView: 1,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "yyyy/mm/dd"
            });

            $('#data_3 .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "yyyy/mm/dd"
            });

            $('#data_4 .input-group.date').datepicker({
                minViewMode: 1,
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                todayHighlight: true
            });

            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

            $('.demo1').click(function(){
            swal({
                title: "Masih ada kolom yang kosong!",
                text: "mohon isi kolom",
                //type: "success",
                closeOnConfirm: true,

            }/*
            function(isConfirm){
              if(isConfirm){
                swal("Selamat","Data berhasil disimpan","Succes");
                window.location="index.php?r=daftar/create";
              }else{
                swal("apa","apa...","bismillah");
              }
            }*/
          );
        });


        });
    </script>
</body>

</html>
