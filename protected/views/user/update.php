<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>User</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Data master</strong>
					</li>
						<li>
								<strong>Travel</strong>
						</li>
						<li>
								<strong>Edit Data</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">

				</div>
		</div>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>