<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>User</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Data master</strong>
					</li>
						<li>
								<strong>User</strong>
						</li>
						<li>
								<strong>Lihat Detail</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">

				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">


			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					                <h2 class="widget style1 navy-bg text-center">Detail Data User</h2>
					            </div>
					            <div class="ibox-content">
								<div class="xxx">
								<table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            
					            <tr class="gradeX">
					                <th>id user</th>
					                <td><?php echo $model->id_user;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>password</th>
					                <td><?php echo $model->password;?></td>
					            </tr>
					            <tr class="gradeX">
					                <th>level</th>
					                <td><?php echo $model->level;?></td>
					            </tr>



						    	</table>
								<a href="index.php?r=user/admin"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-long-arrow-right"></i> NEXT</button></a>
								
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>

</div>
</div>