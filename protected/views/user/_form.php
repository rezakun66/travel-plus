<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<div class="wrapper wrapper-content">
    <div class="text-left animated fadeInRightBig">

    	<div class="row">
		    <div class="col-lg-12">
	            <div class="ibox float-e-margins">
		            <div class="ibox-title">
		                <h2 class="widget style1 navy-bg text-center">Form Data User</h2>
		            </div>
		            <div class="ibox-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'id_user'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'id_user',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'id_user'); ?>
	</div>
	</div>
	</div>


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'password'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->passwordField($model,'password',array('size'=>15,'maxlength'=>15,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	</div>
	</div>


	<div class="row">
		<div class="form-group"><label class="col-sm-2 control-label"> <?php echo $form->labelEx($model,'level'); ?> </label>
		 	<div class="col-sm-6">
		<?php echo $form->textField($model,'level',array('size'=>10,'maxlength'=>10,'class'=>'form-control m-b')); ?>
		<?php echo $form->error($model,'level'); ?>
	</div>
	</div>
	</div>

	<div class="row buttons">
		<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Save', array('class'=>'btn btn-primary')); ?>
	</div>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->
</div><!-- form -->