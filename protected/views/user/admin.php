<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-4">
				<h2>User</h2>
				<ol class="breadcrumb">
					<li class="active">
							<strong>Data master</strong>
					</li>
						<li>
								<strong>User</strong>
						</li>

				</ol>
		</div>
		<div class="col-sm-8">
				<div class="title-action">

				</div>
		</div>
</div>
<div class="wrapper wrapper-content">
		<div class="animated fadeInRightBig">



			<div class="row">
					    <div class="col-lg-12">
				            <div class="ibox float-e-margins">
					            <div class="ibox-title">
					               <h2 class="widget style1 navy-bg text-center">Data Master User</h2>
					            </div>
					            <div class="ibox-content">

					            <a href="index.php?r=user/create"><button type="button" class="btn btn-w-m btn-primary"><i class="fa fa-plus-square"></i>Tambah Data</button></a>
								<div class="xxx">
					            <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" >

					            <thead>
					            <tr>
					                <th>No</th>
					                <th>id user</th>
					                <th>password</th>
					                <th>level</th>
					               
					                <th>action</th>
					            </tr>
					            </thead>

						    	<tbody>
						    	<?php
							    	$nomor=1;
							    	foreach($model as $data){
						    	?>
						    		<tr class="gradeX">
						    			<td><?php echo $nomor;?></td>
						    			<td><?php echo $data->id_user; ?></td>
						    			<td><?php echo $data->password; ?></td>
						    			<td><?php  echo $data->level;	?></td>
									<td width="127" class="text-right">
									<div class="tooltip-demo">
                                        <div class="btn-group">
                                            <a href="index.php?r=user/view&id=<?php echo $data->id_user;?>"> <button class="btn-success btn btn-xs" data-toggle="tooltip" data-placement="left" title="Lihat Detail" data-original-title="Tooltip on left" >Lihat</button></a>
                                            <a href="index.php?r=user/update&id=<?php echo $data->id_user;?>"> <button class="btn-warning btn btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Data" data-original-title="Tooltip on top" >Edit</button></a>
                                            <a href="index.php?r=user/delete&id=<?php echo $data->id_user;?>"> <button class="btn-danger btn btn-xs" data-toggle="tooltip" data-placement="right" title="Hapus Data" data-original-title="Tooltip on right" >Hapus</button></a>
                                        </div>
                                        </div>
                                    </td>

					            	</tr>
					            	<?php
						    	$nomor++;
						    	}
						    	?>
						    	</tbody>


						    	</table>
					            </div>
					            </div>
					            </div>
					            </div>
					            </div>






</div>
</div>