<?php

class PencairanSaldoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','riwayatTravel'),
			'expression'=>'Yii::app()->user->isTravel',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('validasi','riwayatAdmin','formValidasi'),
			'expression'=>'Yii::app()->user->isAdmin',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PencairanSaldo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PencairanSaldo']))
		{
			$model->attributes=$_POST['PencairanSaldo'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pencairan));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PencairanSaldo']))
		{
			$model->attributes=$_POST['PencairanSaldo'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pencairan));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	public function actionFormValidasi($id)
	{
		$model=$this->loadModel($id);
		
		$modelTrav=Travel::model()->findByPk($model->id_travel);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PencairanSaldo']))
		{
			$model->attributes=$_POST['PencairanSaldo'];
			$model->tanggal_transfer=$_POST['tgl_trf'];
			$model->jam_transfer=$_POST['jam_trf'];
			$model->waktu_selesai=date("Y-m-d H:i:s");
			$modelTrav->saldo=$modelTrav->saldo-$model->jumlah_pencairan;


			 $idtop=$model->id_pencairan;	
			$model->bukti_transfer='-';
			if(CUploadedFile::getInstance($model,'bukti_transfer'))
			{
				$newfilename='bukti'.$idtop.'.jpg';
				$model->bukti_transfer=CUploadedFile::getInstance($model,'bukti_transfer');
				$model->bukti_transfer->saveAs(Yii::getPathOfAlias('webroot').'/images/pencairan/'.$newfilename);
				$model->bukti_transfer=$newfilename;
				// $model->save();
			}
			$modelTrav->save();	
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pencairan));
		}

		$this->render('formValidasi',array(
			'model'=>$model,
			'modelTrav'=>$modelTrav,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PencairanSaldo');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PencairanSaldo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PencairanSaldo']))
			$model->attributes=$_GET['PencairanSaldo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionRiwayatTravel()
	{
		
		$id=Yii::app()->session->get('id');
		$model=PencairanSaldo::model()->findAll(array('condition'=>'id_travel="'.$id.'"'));
		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_pencairan) as jumlah from pencairan_saldo where id_travel = '".$id."'";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);
		$this->render('riwayatTravel',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}
	public function actionValidasi()
	{
		//$id=Yii::app()->session->get('id');
		$model=PencairanSaldo::model()->findAll(array('condition'=>'status="diproses"'));
		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_pencairan) as jumlah from pencairan_saldo where status = 'diproses'";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);
		$this->render('validasi',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}
	public function actionRiwayatAdmin()
	{
		//$id=Yii::app()->session->get('id');
		$model=PencairanSaldo::model()->findAll();
		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_pencairan) as jumlah from pencairan_saldo";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);
		$this->render('riwayatAdmin',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PencairanSaldo the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PencairanSaldo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PencairanSaldo $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pencairan-saldo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
