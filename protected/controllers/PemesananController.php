<?php

class PemesananController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('tambahPesan','update','riwayat','pilihLokasi','simpan','detailPesan','batal','batal2','batal3'),
			'expression'=>'Yii::app()->user->isPelanggan',
			),
			
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pemesanan;
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pemesanan']))
		{
			$model->attributes=$_POST['Pemesanan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pesan));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionTambahPesan()
	{
		$this->layout='//layouts/main';
		$model=new Pemesanan;
		$model2=new Kursi;
		$chrcc="a";
		$chr1="a";
		$chr2="a";
		$chr3="a";
		$chr4="a";
		$count=0;
		$kurr= array();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pemesanan']))
		{
			$model->attributes=$_POST['Pemesanan'];
			$model2->attributes=$_POST['Kursi'];
			
			$model->id_pesan=Pemesanan::model()->lastPsn();
			//$model->id_pesan=5;
			
			if(isset($_POST['kursi4'])){
				$chr4=$_POST['kursi4'];
				if($chr4=="ayam"){
					
					$kurr[$count]="4";
					$count++;
				}
			}
			if(isset($_POST['kursiCC'])){
				$chrcc=$_POST['kursiCC'];
				if($chrcc=="cc"){
					
					$kurr[$count]="cc";
					$count++;
				}
			}
			if(isset($_POST['kursi1'])){
				$chr1=$_POST['kursi1'];
				if($chr1=="1"){
					
					$kurr[$count]="1";
					$count++;
				}
			}
			if(isset($_POST['kursi2'])){
				$chr2=$_POST['kursi2'];
				if($chr2=="2"){
					
					$kurr[$count]="2";
					$count++;
				}
			}
			if(isset($_POST['kursi3'])){
				$chr3=$_POST['kursi3'];
				if($chr3=="3"){
					
					$kurr[$count]="3";
					$count++;
				}
			}
			
		 $criteria=new CDbCriteria;
         $criteria->select='id_kursi';
         $criteria->order='id_kursi DESC';
         $criteria->limit=1;
         $modelkursi=Kursi::model()->find($criteria);
		 $modelJdwl=Jadwal::model()->findByPk($model->id_jadwal);
		 $tott=($modelJdwl->price['harga']+$modelJdwl->naik_turun_harga)*$count;
		 $model->pembayaran=$tott;
		 
		 $itung=$modelkursi->id_kursi;
		 $aa=0;
			for( $i=1; $i <= $count; $i++ ){
				$itung++;
				
				$model2->setIsNewRecord(true);
				$model2->id_kursi=$itung;
				$model2->kursi=$kurr[$aa];
				$model2->id_jadwal=$model->id_jadwal;
				$model2->id_pesan=$model->id_pesan;
				$model2->status="proses";
				$model2->save();
				$aa++;	
			}
			
			if($model->save()){
				
				
			//$itung=Pemesanan::model()->lastChair($model2->id_jadwal);
		
				
				$this->redirect(array('pilihLokasi','id'=>$model->id_pesan));
			}
		}

		$this->render('tambahPesan',array(
			'model'=>$model,
			'model2'=>$model2,
		));
	}
	
	public function actionPilihLokasi($id)
	{
		$this->layout='//layouts/main';
		$this->render('pilihLokasi',array(
			'model'=>$this->loadModel($id),
		));
	}	
	public function actionSimpan($id,$lat,$lon)
	{
		$model=$this->loadModel($id);
		$nope=Yii::app()->session->get('id');
		$modelPel=Pelanggan::model()->findByPk($nope);
		if($model->pembayaran>$modelPel->saldo){

			$this->redirect(array('batal3','id'=>$id));
		}
		$idPsn=$model->id_pesan;
		$model->lat_jemput=$lat;
		$model->long_jemput=$lon;
		$model->status="lunas";
		$model->save();
		
		
		$modelTrav=Travel::model()->findByPk($model->schedule->car['id_travel']);
		
		$saldoAwal=$modelPel->saldo;
		$modelPel->saldo=$saldoAwal-$model->pembayaran;
		$modelTrav->saldo=$modelTrav->saldo+$model->pembayaran;
		$modelPel->save();
		$modelTrav->save();
		
		$modelKur=Kursi::model()->findAll(array('condition'=>'id_pesan="'.$idPsn.'"'));
		foreach($modelKur as $kursi){
			$idK=$kursi->id_kursi;
			
			$Kurr=$this->loadModel2($idK);
			$Kurr->status="sistem";
			$Kurr->save();
			
			// send push notif here
			
		}
			
		//$this->layout='//layouts/main';
		$this->redirect(array('riwayat'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pemesanan']))
		{
			$model->attributes=$_POST['Pemesanan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pesan));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionDetailPesan($id)
	{
		$this->layout='//layouts/main';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);


		$this->render('detailPesan',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionBatal($id)
	{
		$this->layout='//layouts/main';
		$modelPsn=$this->loadModel($id);
		$idPsn=$modelPsn->id_pesan;
		$modelKur=Kursi::model()->findAll(array('condition'=>'id_pesan="'.$idPsn.'"'));
		foreach($modelKur as $kursi){
			$idK=$kursi->id_kursi;
			
			$this->loadModel2($idK)->delete();
		}
		$this->loadModel($id)->delete();
		//$this->render('batal');
		$this->redirect("index.php?r=dashboard/dashboard");
	}
	public function actionBatal2($id)
	{
		$modelPsn=$this->loadModel($id);
		$nope=Yii::app()->session->get('id');
		$modelTrav=Travel::model()->findByPk($modelPsn->schedule->car['id_travel']);
		$modelPel=Pelanggan::model()->findByPk($nope);
		$saldoAwal=$modelPel->saldo;
		$saldoRefund=$modelPsn->pembayaran/2;
		$modelPel->saldo=$saldoAwal+$saldoRefund;
		$modelTrav->saldo=$modelTrav->saldo-$saldoRefund;

		
		$modelPsn->waktu_batal=date('Y-m-d h:i:s');
		$modelPsn->pembayaran=$saldoRefund;

		$modelPel->save();
		$modelTrav->save();
		
		$idPsn=$modelPsn->id_pesan;
		$modelKur=Kursi::model()->findAll(array('condition'=>'id_pesan="'.$idPsn.'"'));
		foreach($modelKur as $kursi){
			$idK=$kursi->id_kursi;
			$modelKur2=$this->loadModel2($idK);
			$modelKur2->status="batal";
			$modelKur2->save();
		}
		$modelPsn->status="batal";
		$modelPsn->save();
		$this->redirect("index.php?r=pemesanan/riwayat");
	}

	public function actionBatal3($id)
	{
		$this->layout='//layouts/main';
		$modelPsn=$this->loadModel($id);
		$idPsn=$modelPsn->id_pesan;
		$modelKur=Kursi::model()->findAll(array('condition'=>'id_pesan="'.$idPsn.'"'));
		foreach($modelKur as $kursi){
			$idK=$kursi->id_kursi;
			
			$this->loadModel2($idK)->delete();
		}
		$this->loadModel($id)->delete();
		//$this->render('batal');
		Yii::app()->user->setFlash('saldoKurang', "saldo Anda Kurang");
		$this->redirect("index.php?r=dashboard/dashboard");
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pemesanan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pemesanan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pemesanan']))
			$model->attributes=$_GET['Pemesanan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionRiwayat()
	{
		$this->layout='//layouts/main';
		$nope=Yii::app()->session->get('id');
		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_pesan) as jumlah from pemesanan where id_pelanggan = '".$nope."'";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);
		
		$id=Yii::app()->session->get('id');
		$model=Pemesanan::model()->findAll(array('condition'=>'id_pelanggan="'.$id.'"'));

		$this->render('riwayat',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pemesanan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pemesanan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadModel2($id)
	{
		$model=Kursi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pemesanan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pemesanan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
