<?php

class JadwalController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','update','admin','delete','delete2'),
			'expression'=>'Yii::app()->user->isTravel',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('jadwalSupir','jadwalSupir2','lihatLokasi'),
			'expression'=>'Yii::app()->user->isSupir',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function actionJadwalSupir()
	{
		$driv=Yii::app()->session->get('id');
		$model=Jadwal::model()->findAll(array('condition'=>'supir="'.$driv.'"'));
		$this->render('jadwalSupir',array(
			'model'=>$model,
		));
	}
	public function actionJadwalSupir2($tgl,$tgl2)
	{
		$driv=Yii::app()->session->get('id');
		$criteria=new CDbCriteria;
		
		$criteria->condition = "supir = $driv";        
		$criteria->addBetweenCondition("tanggal",$tgl,$tgl2,'AND');
		//$criteria->compare('supir', $driv)
		$model=Jadwal::model()->findAll($criteria);
		$this->render('jadwalSupir',array(
			'model'=>$model,
		));
	}

	public function actionLihatLokasi($tgl)
	{
		$jdwlid=2;
		$driv=Yii::app()->session->get('id');
		$modelJdwl=Jadwal::model()->findAll(array('condition'=>'supir="'.$driv.'" and tanggal="'.$tgl.'"'));
		foreach($modelJdwl as $data){
			$jdwlid=$data->id_jadwal;
		}
		$model=Pemesanan::model()->findAll(array('condition'=>'id_jadwal="'.$jdwlid.'" and status="lunas"'));
		$this->render('lihatLokasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($tgl)
	{
		$model=new Jadwal;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jadwal']))
		{
			$model->attributes=$_POST['Jadwal'];
						$model->mobil=$_POST['cars'];
			$model->id_harga=$_POST['tujuan'];
			$model->supir=$_POST['driver'];
			if($model->save())
				$this->redirect(array('create','tgl'=>$tgl));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
			$mobilLama=$model->mobil;
			$tujuanLama=$model->id_harga;
			$supirLama=$model->supir;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jadwal']))
		{
			$model->attributes=$_POST['Jadwal'];
			$model->mobil=$_POST['cars'];
			$model->id_harga=$_POST['tujuan'];
			$model->supir=$_POST['driver'];
			if($model->mobil=="x"){
				$model->mobil=$mobilLama;
			}
			if($model->id_harga=="x"){
				$model->id_harga=$tujuanLama;
			}
			if($model->supir=="x"){
				$model->supir=$supirLama;
			}
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_jadwal));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionDelete2($id,$tgl)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(array('create','tgl'=>$tgl));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Jadwal');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$trav=Yii::app()->session->get('id');

		$model=Jadwal::model()->with('car')
		->findAll(array(
                            
                            'condition'=>'car.id_travel="'.$trav.'" '
                    ));
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Jadwal the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Jadwal::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Jadwal $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='jadwal-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
