<?php

class DashboardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('dashboard','dashboardSupir','dashboardAdmin','dashboardTravel','update','jadwal'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new HargaTujuan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HargaTujuan']))
		{
			$model->attributes=$_POST['HargaTujuan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_harga));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HargaTujuan']))
		{
			$model->attributes=$_POST['HargaTujuan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_harga));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('HargaTujuan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	public function actionDashboard()
	{
		$level=Yii::app()->session->get('level');
		if($level=='admin'){
			$this->redirect(array('dashboardAdmin'));
		}else if($level=='supir'){
			$this->redirect(array('dashboardSupir'));
		}else if($level=='travel'){
			$this->redirect(array('dashboardTravel'));
		}
		$this->render('dashboard');
	}
	
	public function actionDashboardAdmin()
	{
		$this->layout='//layouts/dashboard';
		$timee=" 23:59:59";
		$timee0=" 00:00:00";
		$tgl=date('Y-m-d');

		$modeldb=Yii::app()->db;
		$queryJumlahTopUp="select count(id_top_up) as jumlah from log_top_up where status='diterima' and waktu_top_up between '".$tgl."".$timee0."' and '".$tgl."".$timee."'";
		$queryJumlahCair="select count(id_pencairan) as jumlah from pencairan_saldo where status='ditranfer' and waktu_pencairan between '".$tgl."".$timee0."' and '".$tgl."".$timee."'";

		$sqlJumlahTopUp=$modeldb->createCommand($queryJumlahTopUp)->query();
		$sqlJumlahCair=$modeldb->createCommand($queryJumlahCair)->query();

		foreach($sqlJumlahTopUp as $sqlJumlahTopUp1);
		foreach($sqlJumlahCair as $sqlJumlahCair1);

		$model=LogTopUp::model()
		->findAll(array(     
        'condition'=>'status="diterima" and waktu_top_up between "'.$tgl.$timee0.'" and "'.$tgl.$timee.'" '
                    ));
		$model2=PencairanSaldo::model()
		->findAll(array(     
        'condition'=>'status="ditransfer" and waktu_selesai between "'.$tgl.$timee0.'" and "'.$tgl.$timee.'" '
                    ));
		$this->render('dashboardAdmin',array(
			'model'=>$model,
			'model2'=>$model2,
			'jumlahTopUp'=>$sqlJumlahTopUp1,
			'jumlahCair'=>$sqlJumlahCair1,
		));
	}	


	public function actionDashboardTravel()
	{
		$this->layout='//layouts/dashboard';
		$timee=" 23:59:59";
		$timee0=" 00:00:00";
		$tgl=date('Y-m-d');

		$modeldb=Yii::app()->db;
		$queryJumlahPesan="select count(id_pesan) as jumlah from pemesanan where waktu_pesan between '".$tgl."".$timee0."' and '".$tgl."".$timee."'";
		$queryJumlahBatal="select count(id_pesan) as jumlah from pemesanan where status='batal' and waktu_pesan between '".$tgl."".$timee0."' and '".$tgl."".$timee."'";
		$queryJumlahPembayaran="select sum(pembayaran) as jumlah from pemesanan where waktu_pesan between '".$tgl."".$timee0."' and '".$tgl."".$timee."'";
		$queryJumlahArmada="select count(id_jadwal) as jumlah from jadwal where tanggal = '".$tgl."'";

		$sqlJumlahPesan=$modeldb->createCommand($queryJumlahPesan)->query();
		$sqlJumlahBatal=$modeldb->createCommand($queryJumlahBatal)->query();
		$sqlJumlahPembayaran=$modeldb->createCommand($queryJumlahPembayaran)->query();
		$sqlJumlahArmada=$modeldb->createCommand($queryJumlahArmada)->query();

		foreach($sqlJumlahPesan as $sqlJumlahPesan1);
		foreach($sqlJumlahBatal as $sqlJumlahBatal1);
		foreach($sqlJumlahPembayaran as $sqlJumlahPembayaran1);
		foreach($sqlJumlahArmada as $sqlJumlahArmada1);

		$trav=Yii::app()->session->get('id');
		

		$model=Jadwal::model()->with('car')
		->findAll(array(
                            
                            'condition'=>'car.id_travel="'.$trav.'" and tanggal = "'.$tgl.'"'
                    ));
		//$model=Jadwal::model()->findAll(array('condition'=>'id_travel="'.$trav.'"'));
		$this->render('dashboardTravel',array(
			'jumlahPesan'=>$sqlJumlahPesan1,
			'jumlahBatal'=>$sqlJumlahBatal1,
			'jumlahPembayaran'=>$sqlJumlahPembayaran1,
			'jumlahArmada'=>$sqlJumlahArmada1,
			'model'=>$model,
		));
	}	
	public function actionJadwal()
	{

		$this->render('jadwal');
	}

	public function actionDashboardSupir()
	{
		$this->layout='//layouts/dashboard';
		/*$tgl=date('Y-m-d');
		$driv=Yii::app()->session->get('id');
		$modelJdwl=Jadwal::model()->findAll(array('condition'=>'supir="'.$driv.'" and tanggal="'.$tgl.'"'));
		foreach($modelJdwl as $data){
			$model=Pemesanan::model()->findAll(array('condition'=>'id_jadwal="'.$data->id_jadwal.'" and status="lunas"'));
		}*/
		$this->render('dashboardSupir');
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new HargaTujuan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['HargaTujuan']))
			$model->attributes=$_GET['HargaTujuan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return HargaTujuan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=HargaTujuan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param HargaTujuan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='harga-tujuan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
