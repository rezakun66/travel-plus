<?php

class LaporanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('laporanTravel','update','laporanAdmin'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PencairanToken;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PencairanToken']))
		{
			$model->attributes=$_POST['PencairanToken'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->waktu_pencairan));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PencairanToken']))
		{
			$model->attributes=$_POST['PencairanToken'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->waktu_pencairan));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PencairanToken');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	public function actionLaporanTravel($tgl,$tgl2)
	{
		$this->layout='//layouts/dashboard';
		$timee=" 23:59:59";
		$timee0=" 00:00:00";
		//$tgl=date('Y-m-d');

		$modeldb=Yii::app()->db;
		$queryJumlahPesan="select count(id_pesan) as jumlah from pemesanan where waktu_pesan between '".$tgl."".$timee0."' and '".$tgl2."".$timee."'";
		$queryJumlahBatal="select count(id_pesan) as jumlah from pemesanan where status='batal' and waktu_pesan between '".$tgl."".$timee0."' and '".$tgl2."".$timee."'";
		$queryJumlahPembayaran="select sum(pembayaran) as jumlah from pemesanan where waktu_pesan between '".$tgl."".$timee0."' and '".$tgl2."".$timee."'";
		$queryJumlahRefund="select sum(pembayaran) as jumlah from pemesanan where status='batal' and waktu_pesan between '".$tgl."".$timee0."' and '".$tgl2."".$timee."'";
		$queryJumlahArmada="select count(id_jadwal) as jumlah from jadwal where tanggal between '".$tgl."' and '".$tgl2."'";

		$sqlJumlahPesan=$modeldb->createCommand($queryJumlahPesan)->query();
		$sqlJumlahBatal=$modeldb->createCommand($queryJumlahBatal)->query();
		$sqlJumlahPembayaran=$modeldb->createCommand($queryJumlahPembayaran)->query();
		$sqlJumlahRefund=$modeldb->createCommand($queryJumlahRefund)->query();
		$sqlJumlahArmada=$modeldb->createCommand($queryJumlahArmada)->query();

		foreach($sqlJumlahPesan as $sqlJumlahPesan1);
		foreach($sqlJumlahBatal as $sqlJumlahBatal1);
		foreach($sqlJumlahPembayaran as $sqlJumlahPembayaran1);
		foreach($sqlJumlahRefund as $sqlJumlahRefund1);
		foreach($sqlJumlahArmada as $sqlJumlahArmada1);

		$trav=Yii::app()->session->get('id');
		

		$model=Jadwal::model()->with('car')
		->findAll(array(
                            
                            'condition'=>'car.id_travel="'.$trav.'" and tanggal between "'.$tgl.'" and "'.$tgl2.'" '
                    ));
		$model2=Pemesanan::model()
		->findAll(array(
                            
                            'condition'=>' waktu_pesan between "'.$tgl.$timee0.'" and "'.$tgl2.$timee.'" '
                    ));
		$model3=Pemesanan::model()
		->findAll(array(
                            
                            'condition'=>'status="batal" and waktu_pesan between "'.$tgl.$timee0.'" and "'.$tgl2.$timee.'" '
                    ));
		//$model=Jadwal::model()->findAll(array('condition'=>'id_travel="'.$trav.'"'));
		$this->render('laporanTravel',array(
			'jumlahPesan'=>$sqlJumlahPesan1,
			'jumlahBatal'=>$sqlJumlahBatal1,
			'jumlahPembayaran'=>$sqlJumlahPembayaran1,
			'jumlahRefund'=>$sqlJumlahRefund1,
			'jumlahArmada'=>$sqlJumlahArmada1,
			'model'=>$model,
			'model2'=>$model2,
			'model3'=>$model3,
		));
	}

	public function actionLaporanAdmin($tgl,$tgl2)
	{
		$this->layout='//layouts/dashboard';
		$timee=" 23:59:59";
		$timee0=" 00:00:00";
		//$tgl=date('Y-m-d');

		$modeldb=Yii::app()->db;
		$queryJumlahTopUp="select count(id_top_up) as jumlah from log_top_up where status='diterima' and waktu_top_up between '".$tgl."".$timee0."' and '".$tgl2."".$timee."'";
		$queryJumlahCair="select count(id_pencairan) as jumlah from pencairan_saldo where status='ditransfer' and waktu_pencairan between '".$tgl."".$timee0."' and '".$tgl2."".$timee."'";

		$sqlJumlahTopUp=$modeldb->createCommand($queryJumlahTopUp)->query();
		$sqlJumlahCair=$modeldb->createCommand($queryJumlahCair)->query();

		foreach($sqlJumlahTopUp as $sqlJumlahTopUp1);
		foreach($sqlJumlahCair as $sqlJumlahCair1);

		$model=LogTopUp::model()
		->findAll(array(     
        'condition'=>'status="diterima" and waktu_top_up between "'.$tgl.$timee0.'" and "'.$tgl2.$timee.'" '
                    ));
		$model2=PencairanSaldo::model()
		->findAll(array(     
        'condition'=>'status="ditransfer" and waktu_selesai between "'.$tgl.$timee0.'" and "'.$tgl2.$timee.'" '
                    ));
		$this->render('laporanAdmin',array(
			'model'=>$model,
			'model2'=>$model2,
			'jumlahTopUp'=>$sqlJumlahTopUp1,
			'jumlahCair'=>$sqlJumlahCair1,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PencairanToken('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PencairanToken']))
			$model->attributes=$_GET['PencairanToken'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PencairanToken the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PencairanToken::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PencairanToken $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pencairan-token-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
