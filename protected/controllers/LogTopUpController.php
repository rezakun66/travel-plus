<?php
//require_once(Yii::getPathOfAlias('webroot').'/protected/vendor/vendor/autoload.php');

class LogTopUpController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','test'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','riwayat'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','update','riwayat','view','delete2','admin','daftarHarga'),
			'expression'=>'Yii::app()->user->isPelanggan',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('formValidasi','validasi','riwayat','view','delete2','admin','daftarHarga','tolak','terima'),
			'expression'=>'Yii::app()->user->isAdmin',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionTest(){
		$this->layout='//layouts/main';
		$pusher=Yii::app()->pusher2;
		//$pusher->trigger('messaging','newMessage',array('msg'=>'Ular melingkar dipagar bundar'));
				  $data['message'] = 'hello world';
  $pusher->trigger('my-channel', 'my-event', $data);
  
  echo "<pre>";
  var_dump($pusher);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout='//layouts/main';
		$model=new LogTopUp;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LogTopUp']))
		{
			$model->attributes=$_POST['LogTopUp'];
			
		 $criteria=new CDbCriteria;
         $criteria->select='id_top_up';
         $criteria->order='id_top_up DESC';
         $criteria->limit=1;
         $modelTop=LogTopUp::model()->find($criteria);
		 $idtop=$modelTop->id_top_up+1;	
			$model->bukti_transfer='-';
			if(CUploadedFile::getInstance($model,'bukti_transfer'))
			{
				$newfilename='bukti'.$idtop.'.jpg';
				$model->bukti_transfer=CUploadedFile::getInstance($model,'bukti_transfer');
				$model->bukti_transfer->saveAs(Yii::getPathOfAlias('webroot').'/images/topup/'.$newfilename);
				$model->bukti_transfer=$newfilename;
				// $model->save();
			}			
			
			if($model->save()){
				
				// send push notif here
/*				$pusher=Yii::app()->pusher2;
		//$pusher->trigger('messaging','newMessage',array('msg'=>'Ular melingkar dipagar bundar'));
				  $data['message'] = 'hello world';
  $pusher->trigger('my-channel', 'my-event', $data);}*/
				$this->redirect(array('riwayat'));
				}
		
			}
		$this->render('create',array(
			'model'=>$model,
		));
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		$lev=Yii::app()->session->get('level');
		if($lev=="pelanggan"){
			$this->layout='//layouts/main';
		}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LogTopUp']))
		{
			$model->attributes=$_POST['LogTopUp'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->waktu_top_up));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	public function actionFormValidasi($id)
	{
		$model=$this->loadModel($id);
		$modelCus=Pelanggan::model()->findByPk($model->id_user);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LogTopUp']))
		{
			$model->attributes=$_POST['LogTopUp'];
			if($model->status=="diterima"){
				$modelCus->saldo=$modelCus->saldo+$model->jumlah;
				$modelCus->save();
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_top_up));
		}

		$this->render('formValidasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionDelete2($id)
	{
		$this->loadModel2($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('LogTopUp');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		
		$model=new LogTopUp('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LogTopUp']))
			$model->attributes=$_GET['LogTopUp'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionRiwayat()
	{
		$this->layout='//layouts/main';
		$id=Yii::app()->session->get('id');
		$model=LogTopUp::model()->findAll(array('condition'=>'id_user="'.$id.'"'));
		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_top_up) as jumlah from log_top_up where id_user = '".$id."'";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);
		$this->render('riwayat',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}

	public function actionValidasi()
	{
		//$this->layout='//layouts/dashboard';
		$model=LogTopUp::model()->findAll(array('condition'=>'status="diproses"'));

		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_top_up) as jumlah from log_top_up where status = 'diproses'";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);

		$this->render('validasi',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}
	public function actionTerima()
	{
		//$this->layout='//layouts/dashboard';
		$model=LogTopUp::model()->findAll(array('condition'=>'status="diterima"'));

		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_top_up) as jumlah from log_top_up where status = 'diterima'";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);

		$this->render('validasi',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}
	public function actionTolak()
	{
		//$this->layout='//layouts/dashboard';
		$model=LogTopUp::model()->findAll(array('condition'=>'status="ditolak"'));

		$modeldb=Yii::app()->db;
		$queryJumlah="select count(id_top_up) as jumlah from log_top_up where status = 'ditolak'";
		$sqlJumlah=$modeldb->createCommand($queryJumlah)->query();
		foreach($sqlJumlah as $sqlJumlah1);

		$this->render('validasi',array(
			'model'=>$model,
			'jumlah'=>$sqlJumlah1,
		));
	}
	public function actionDaftarHarga()
	{
		$this->layout='//layouts/main';
		$this->render('daftarHarga');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LogTopUp the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LogTopUp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadModel2($id)
	{
		$model=LogTopUp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LogTopUp $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='log-top-up-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
