<?php

class SupirController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','update','admin','delete'),
			'expression'=>'Yii::app()->user->isTravel',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','update','admin','profile'),
			'expression'=>'Yii::app()->user->isSupir',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Supir;
		$model2=new User;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Supir']))
		{
			$model->attributes=$_POST['Supir'];
			$model2->attributes=$_POST['User'];
			
			$model->no_telp=$model->id_supir;
			$model2->id_user=$model->no_telp;
			$model2->password=$model->no_telp;
			$model2->level="supir";
			
			$model->foto='-';
			if(CUploadedFile::getInstance($model,'foto'))
			{
				$newfilename='foto'.$model->no_telp.'.jpg';
				$model->foto=CUploadedFile::getInstance($model,'foto');
				$model->foto->saveAs(Yii::getPathOfAlias('webroot').'/images/supir/'.$newfilename);
				$model->foto=$newfilename;
				// $model->save();
			}
			
			if($model->save()){
				$model2->save();
				$this->redirect(array('view','id'=>$model->id_supir));
				}
		}

		$this->render('create',array(
			'model'=>$model,
			'model2'=>$model2,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Supir']))
		{
			$model->attributes=$_POST['Supir'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_supir));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	public function actionProfile()
	{
		$nope=Yii::app()->session->get('id');
		$model=$this->loadModel($nope);
		$model2=User::model()->findByPk($nope);
		$fotoLama=$model->foto;
		if(isset($_POST['Supir']))
		{
			$model->attributes=$_POST['Supir'];
			$model2->attributes=$_POST['User'];
			
			//$model->no_telp=$model->id_supir;
			//$model2->id_user=$model->no_telp;
			//$model2->password=$model->no_telp;
			//$model2->level="supir";
			
			$model->foto=$fotoLama;
			if(CUploadedFile::getInstance($model,'foto'))
			{
				$newfilename='foto'.$model->no_telp.'.jpg';
				$model->foto=CUploadedFile::getInstance($model,'foto');
				$model->foto->saveAs(Yii::getPathOfAlias('webroot').'/images/supir/'.$newfilename);
				$model->foto=$newfilename;
				// $model->save();
			}
			
			if($model->save()){
				$model2->save();
				$this->redirect("index.php?r=dashboard/dashboardSupir");
				}
		}

		$this->render('profile',array(
			'model'=>$model,
			'model2'=>$model2,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Supir');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$id=Yii::app()->session->get('id');
		$model=Supir::model()->findAll(array('condition'=>'id_travel="'.$id.'"'));

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Supir the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Supir::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Supir $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='supir-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
