<?php
$hashed = '$2y$10$XVcj98iqz5qUMym4/D812e21yWeq4Mb/YOj039/nXoZ1epQXGcHNa';
$hashed2 = '$2y$10$quEgNd/4v6/3jy9ZixQw8OlCSsvYMrijPgh1tYk4BwQUQW86mrJnS';
 
if (password_verify('admin12345', $hashed2)) {
    echo 'Password is valid!';
} else {
    echo 'Invalid password.';
}
?>